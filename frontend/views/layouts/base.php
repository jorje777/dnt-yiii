<?php

use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;

/* @var $this \yii\web\View */
/* @var $content string */

$this->beginContent('@frontend/views/layouts/_clear.php')
?>
    <div class="wrap">
        <?php
        NavBar::begin([
            'innerContainerOptions' => [
                'class' => 'container',
            ],
            'options' => [
                'class' => 'navbar-default navbar-fixed-top',
            ],
        ]); ?>
        <?php echo Nav::widget([
            'options' => [
                'class' => 'navbar-nav',
            ],
            'items' => [
                [
                    'label' => Yii::t('dnt', 'პაციენტი'),
                    'visible'=> !Yii::$app->user->isGuest,
                    'items' => [
                        ['label' => Yii::t('dnt', 'პაციენტის დამატება'), 'url' => ['/dnt/patient/create'],'visible'=>!Yii::$app->user->isGuest],
                        ['label' => Yii::t('dnt', 'პაციენტის სია'), 'url' => ['/dnt/patient/index'],'visible'=>!Yii::$app->user->isGuest],
                    ]
                ],
                [
                        'label' => Yii::t('dnt', 'Log In'),
                    'url' => ['/user/sign-in/login'],
                    'visible'=>Yii::$app->user->isGuest],
                [
                    'label' => Yii::t('dnt', 'პროტოკოლი'),
                    'visible'=>!Yii::$app->user->isGuest,
                    'items' => [
                        [
                            'label' => Yii::t('dnt', 'სახელმწიფო'),
                            'url' => ['/dnt/protocol/create','type' => \frontend\modules\dnt\models\Drug::TYPE_STATE],
                            'visible'=>!Yii::$app->user->isGuest
                        ],
                        [
                            'label' => Yii::t('dnt', 'სახელმწიფო მწვავე'),
                            'url' => ['/dnt/protocol/create','type' => \frontend\modules\dnt\models\Drug::TYPE_STATE_MWVAVE],
                            'visible'=> (!Yii::$app->user->isGuest && (!Yii::$app->user->can(\common\models\User::ROLE_USER) || Yii::$app->user->can(\common\models\User::ROLE_MANAGER))),
                        ],
                    ]
                ],
                [
                    'label' => Yii::t('dnt', 'პროტოკოლის სია'),
                    'visible'=>!Yii::$app->user->isGuest,
                    'items' => [
                        ['label' => Yii::t('dnt', 'სახელმწიფო'), 'url' => ['/dnt/protocol/index','type' => \frontend\modules\dnt\models\Drug::TYPE_STATE],'visible'=>!Yii::$app->user->isGuest],
                        [
                            'label' => Yii::t('dnt', 'სახელმწიფო მწვავე'),
                            'url' => ['/dnt/protocol/index', 'type' => \frontend\modules\dnt\models\Drug::TYPE_STATE_MWVAVE],
                            'visible'=> (!Yii::$app->user->isGuest && (!Yii::$app->user->can(\common\models\User::ROLE_USER) || Yii::$app->user->can(\common\models\User::ROLE_MANAGER))),
                        ],
                    ]
                ],
//                ['label' => Yii::t('dnt', 'პრეპარატის კატეგორია'), 'url' => ['/dnt/drug-category/index']],
                [
                    'label' => Yii::t('dnt', 'პრეპარატი'),
                    'visible'=> (!Yii::$app->user->isGuest && (!Yii::$app->user->can(\common\models\User::ROLE_USER) || Yii::$app->user->can(\common\models\User::ROLE_MANAGER))),
                    'items' => [
                        [
                            'label' => "სტანდარტული",
                            'url' => ['/dnt/drug/index','drug_type' => \frontend\modules\dnt\models\Drug::TYPE_STANDARD]
                        ],
                        [
                            'label' => "სახელმწიფო",
                            'url' => ['/dnt/drug/index','drug_type' => \frontend\modules\dnt\models\Drug::TYPE_STATE]
                        ],
                        [
                            'label' => "სახელმწიფო მწვავე",
                            'url' => ['/dnt/drug/index', 'drug_type' => \frontend\modules\dnt\models\Drug::TYPE_STATE_MWVAVE]
                        ]
                    ]
                ],
                [
                    'label' => Yii::t('dnt','მარაგი'),
                    'visible'=> (!Yii::$app->user->isGuest && (!Yii::$app->user->can(\common\models\User::ROLE_USER) || Yii::$app->user->can(\common\models\User::ROLE_MANAGER))),
                    'items' => [
                        [
                            'label' => "სტანდარტული",
                            'url' => ['/dnt/stock/index','drug_type' => \frontend\modules\dnt\models\Drug::TYPE_STANDARD]
                        ],
                        [
                            'label' => "სახელმწიფო",
                            'url' => ['/dnt/stock/index','drug_type' => \frontend\modules\dnt\models\Drug::TYPE_STATE]
                        ],
                        [
                            'label' => "სახელმწიფო მწვავე",
                            'url' => ['/dnt/stock/index', 'drug_type' => \frontend\modules\dnt\models\Drug::TYPE_STATE_MWVAVE]
                        ]
                    ]
                ],
                ['label' => Yii::t('dnt','დანახარჯი'),'url' => ['/dnt/expense/index'],'visible'=>(!Yii::$app->user->isGuest && (!Yii::$app->user->can(\common\models\User::ROLE_USER) || Yii::$app->user->can(\common\models\User::ROLE_MANAGER)))],
                ['label' => Yii::t('dnt','ნაშთი'),'url' => ['/dnt/remainder/index'],'visible'=>(!Yii::$app->user->isGuest && (!Yii::$app->user->can(\common\models\User::ROLE_USER) || Yii::$app->user->can(\common\models\User::ROLE_MANAGER)))],
                [
                    'label' => Yii::t('dnt', 'ფილტრი'),
                    'visible'=>(!Yii::$app->user->isGuest && (!Yii::$app->user->can(\common\models\User::ROLE_USER) || Yii::$app->user->can(\common\models\User::ROLE_MANAGER))),
                    'items' => [
                        [
                            'label' => "რაოდენობა",
                            'url' => ['/dnt/filter/quantity']
                        ],
                        [
                            'label' => "ფასი",
                            'url' => ['/dnt/filter/price']
                        ],
                        [
                            'label' => "პაციენტი",
                            'url' => ['/dnt/filter/patients']
                        ],
                        [
                            'label' => "პროტოკოლი",
                            'url' => ['/dnt/filter/protocols']
                        ],
                        [
                            'label' => "ნაშთი",
                            'url' => ['/dnt/filter/remainder']
                        ],
                        [
                            'label' => "MOH",
                            'url' => ['/dnt/filter/moh']
                        ],
                        [
                            'label' => "MOH - time",
                            'url' => ['/dnt/filter/moh-time']
                        ],
                        [
                            'label' => 'ნატას რეპორტი',
                            'url' => ['/dnt/report/moh']
                        ]
                    ]
                ],
                [
                    'label' => Yii::$app->user->isGuest ? '' : Yii::$app->user->identity->getPublicIdentity(),
                    'visible'=>!Yii::$app->user->isGuest,
                    'items'=>[
                        [
                            'label' => Yii::t('frontend', 'გასვლა'),
                            'url' => ['/user/sign-in/logout'],
                            'linkOptions' => ['data-method' => 'post']
                        ]
                    ]
                ],
            ]
        ]); ?>
        <?php NavBar::end(); ?>

        <?php echo $content ?>

    </div>

    <footer class="footer">
        <div class="container">
            <p class="pull-left">&copy; DNT Union <?php echo date('Y') ?></p>
            <p class="pull-right">Copyright 2017. </p>
        </div>
    </footer>
<?php $this->endContent() ?>