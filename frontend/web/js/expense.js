$(document).ready(function(){
    $(".s2-select-label").remove();
    $("#drug_id").on("select2:select", function (response) {
        var data = response.params.data;
        generateDrugInputs(data.id,data.text);
    });

    $("#drug_id").on("select2:unselect", function (response) {
        deleteDrugInput(response.params.data.id);
    });

    $("#generateLab").on("click",function () {
        generateLabInputs();
    });
    $("#removeLab").on("click",function () {
        deleteLabInput();
    });

    $("#generateBlood").on("click",function () {
        generateBloodInputs();
    });
    $("#removeBlood").on("click",function () {
        deleteBloodInput();
    });

    $("#generalAlertBox").fadeTo(2000, 500).slideUp(500, function(){
        $("#generalAlertBox").slideUp(500);
    });

});

function generateDrugInputs(id,text) {
    var div =  $("#drugsGenerateDiv");
    console.log(id);
    var htmlContent =
        "<div class='row' id='drugs_"+id+"'>" +
            "<div class='col-md-3'>" +
                "<div class='form-group'>" +
                    "<label>"+ text +"</label>" +
                    "<input type='number' name='drugs[" + id + "]' class='form-control' required>" +
                "</div>" +
            "</div>" +
        "</div>";
    div.append(htmlContent);
}
function deleteDrugInput(id) {
    var newId = "#drugs_"+id;
    $(newId).remove();
}


var labId = 0;
var bloodLabId = 0;

function generateLabInputs() {
    var div = $("#labGenerateDiv");
    $("#last_generated_lab_input").removeAttr('id');
    
    var htmlContent =
        "<div class='row lab_generates' id='last_generated_lab_input'>" +
            "<div class='col-md-3'>" +
                "<div class='form-group'>" +
                    "<label> Name </label>" +
                    "<input type='text' class='form-control' name='lab[" + labId + "][name]' required>" +
                "</div>" +
            "</div>" +
            "<div class='col-md-3'>" +
                "<div class='form-group'>" +
                    "<label> Quantity </label>" +
                    "<input type='text' class='form-control' name='lab[" + labId + "][quantity]' required>" +
                "</div>" +
            "</div>" +
            "<div class='col-md-3'>" +
                "<div class='form-group'>" +
                    "<label> Price </label>" +
                    "<input type='text' class='form-control' name='lab[" + labId + "][price]' required>" +
                "</div>" +
            "</div>" +
        "</div>";
    div.append(htmlContent);
    labId++;
}
function deleteLabInput(){
    var last = $("#last_generated_lab_input");
    if(typeof last != "undefined" || last != null){
        last.remove();
    }
    $(".lab_generates").last().attr('id',"last_generated_lab_input");
}

function generateBloodInputs() {
    var div = $("#bloodGenerateDiv");
    $("#last_generated_blood_input").removeAttr('id');

    var htmlContent =
        "<div class='row blood_generates' id='last_generated_blood_input'>" +
        "<div class='col-md-3'>" +
        "<div class='form-group'>" +
        "<label> Name </label>" +
        "<input type='text' class='form-control' name='bloodLab[" + bloodLabId + "][name]' required>" +
        "</div>" +
        "</div>" +
        "<div class='col-md-3'>" +
        "<div class='form-group'>" +
        "<label> Quantity </label>" +
        "<input type='text' class='form-control' name='bloodLab[" + bloodLabId + "][quantity]' required>" +
        "</div>" +
        "</div>" +
        "<div class='col-md-3'>" +
        "<div class='form-group'>" +
        "<label> Price </label>" +
        "<input type='text' class='form-control' name='bloodLab[" + bloodLabId + "][price]' required>" +
        "</div>" +
        "</div>" +
        "</div>";
    div.append(htmlContent);
    bloodLabId++;
}
function deleteBloodInput(){
    var last = $("#last_generated_blood_input");
    if(typeof last != "undefined" || last != null){
        last.remove();
    }
    $(".blood_generates").last().attr('id',"last_generated_blood_input");
}


$("#stockPageSubmitButton").on('click',function () {
  if(!$('#stockPagePatientNameField').val()){
    window.alert("აირჩიეთ პაციენტი");
  }
});