$(document).ready(function () {
  $('#remainderDrugTypeDropdown').on('change', function () {
    var drug_type = this.options[this.selectedIndex].value;
    $.post("/dnt/remainder/get-stock-by-drug-type", {
      "drug_type": drug_type,
    }, function (response) {

      var tableBody = $("#remainderStockTableBody");
      tableBody.children().remove();
      if(response.length > 0){
        var drugs = response;
        console.log("drugs", drugs);
        var sum = 0;
        for(var i = 0; i < drugs.length; i++){
          var htmlContent =
            "<tr>" +
            "<td>" + drugs[i].name +"</td>" +
            "<td>" + drugs[i].quantity +"</td>" +
            "</tr>";
          sum += parseInt(drugs[i].quantity);
          tableBody.append(htmlContent);
        }
        tableBody.append(
          "<tr>" +
          "<td></td>" +
          "<td>"+ "ჯამი :" + sum +"</td>" +
          "</tr>"
        );
      }
    });
  });


    $("#excelButtonRemainder").on('click', function (e) {
        e.preventDefault();
        var form = $('#remainderFilterForm');
        form.attr('action', '/dnt/excel/generate-remainder-excel');
        form.submit();
    });

    $("#excelButtonRemainderDefault").on('click', function (e) {
        e.preventDefault();
        var form = $('#remainderFilterForm');
        form.attr('action', '/dnt/filter/remainder');
        form.submit();
    })
});