$(document).ready(function () {
  $('#protocolTimeCheckBox').on('change', function () {
    if(this.checked == true){
      $("#protocolTimeDiv").removeClass('hide');
    }else{
      $("#protocolTimeDiv").addClass('hide');
    }
  });


  var createdInputs = [];

  $('#protocolDrugCheckboxColumns :checkbox').on('change',function () {
    var label = this.nextElementSibling;
    var elementId = "expense" + this.value;

    if(this.checked === true){
      $(generateInputsForDrugs(this.value,elementId)).insertAfter(label);
    }else{
      $("#"+elementId).remove();
      createdInputs.splice(createdInputs.indexOf(elementId));
    }
    label = null;
    elementId = null;

  });

  function generateInputsForDrugs(id,elementId) {
    createdInputs.push(elementId);
    var htmlContent = "<div class='form-group'  id='" + elementId + "'>" +
                        "<input type='number' name='expense_drugs[" + id + "]' class='form-control' required'>" +
                      "</div>";
    return htmlContent;
  }












  $('#protocolModal').on('show.bs.modal', function (e) {
    $('#protocolSubmitButton').prop('disabled', true)
    var elems = $( "[name^='expense_drugs']" );

    if(!$("#protocol-patient_id").val() || !$('#protocol-session_date').val()){
      $('#protocolSubmitButton').prop('disabled', true);
      $('#protocolModalTableBody').empty();
      return;
    }
    $('#protocolSubmitButton').prop('disabled', false);

    if(elems.length > 0){
      var sendArray = [];
      for(var i = 0; i < elems.length; i++){
        var nameAttr = elems[i].name;
        var quantity = elems[i].value;
        var drugId = nameAttr.match(/\d+/)[0];
        sendArray.push({
          'drug_id' : drugId,
          'quantity' : quantity
        });
      }

      $.post("/dnt/protocol/check-drug-quantity", {
        'drugs': sendArray
      }, function (response) {
        if(response.success){
          $('#protocolSubmitButton').prop('disabled', false);
        }
        var tableBody = $('#protocolModalTableBody');

        for(var i = 0; i < response.data.length; i++){
          var cls = "bg-danger";
          if(response.data[i].is_enough){
            cls = "bg-success";
          }
          var html = "<tr class='" + cls +"'>" +
            "<td>"+ response.data[i].name +"</td>" +
            "<td>"+ response.data[i].quantity +"</td>" +
            "<td>"+ response.data[i].stock +"</td>" +
            "</tr>";
          $(html).appendTo(tableBody);
        }

      });

      $('#protocolModalTableBody').empty();
    }

  });



  function protocolPrintClick() {
    var iframe = document.getElementById('customIframe');
    var div = document.getElementsByClassName('table-bordered')[0];
    iframe.contentWindow.document.body.innerHTML =
      "<style> table{border-collapse: collapse;}td,th {border:0.2px solid #cecece}td{text-align: center;padding: 5px} </style>" +

      "<table>" + div.innerHTML + "</table>";
    iframe.contentWindow.print();
  }

  $('#protocolPrintButton').click(function () {
    protocolPrintClick();
  });


});