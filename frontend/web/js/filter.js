$(document).ready(function () {
  $('#filterDrugType').on('change', function () {
    console.log(this.options[this.selectedIndex].value);
    var typeId = this.options[this.selectedIndex].value;
    $.post("/dnt/filter/get-drugs-by-type", {
      "typeId": typeId,
    }, function (response) {
      console.log(response);
      var drugs = response;

      var select = document.getElementById('drug_id');
      if (select) {
        select.innerHTML = "";
      }
      for (var i = 0; i < drugs.length; i++) {
        var option = document.createElement("option");
        console.log(option);
        option.text = drugs[i].name;
        option.value = drugs[i].id;

        select.appendChild(option);
      }
    });
  });


  $("#excelButtonPatients").on('click', function (e) {
    e.preventDefault();
    var form = $('#patientsFilterForm');
    form.attr('action', '/dnt/excel/generate-patients-excel');
    form.submit();
  });
  $("#excelButtonPatientsDefault").on('click', function (e) {
    e.preventDefault();
    var form = $('#patientsFilterForm');
    form.attr('action', '/dnt/filter/patients');
    form.submit();
  });


  $("#excelButtonProtocols").on('click', function (e) {
    e.preventDefault();
    var form = $('#protocolsFilterForm');
    form.attr('action', '/dnt/excel/generate-protocols-excel');
    form.submit();
  });

  $("#excelButtonProtocolsDefault").on('click', function (e) {
    e.preventDefault();
    var form = $('#protocolsFilterForm');
    form.attr('action', '/dnt/filter/protocols');
    form.submit();
  })


  $("#excelButtonMoh").on('click', function (e) {
    e.preventDefault();
    var form = $('#mohFilterForm');
    form.attr('action', '/dnt/excel/generate-moh');
    form.submit();
  });

  $("#excelButtonMohDefault").on('click', function (e) {
    e.preventDefault();
    var form = $('#mohFilterForm');
    form.attr('action', '/dnt/filter/moh');
    form.submit();
  });

  $('#mohAllCheckBox').on('change', function () {
    if (this.checked == true) {
      $(".mohCheckboxes").prop("checked", true);
    }else{
      $(".mohCheckboxes").prop('checked',false)
    }
  });


  $("#buttonReportMohDefault").on('click', function (e) {
    e.preventDefault();
    var form = $('#mohReportForm');
    form.attr('action', '/dnt/report/moh');
    form.submit();
  });

  $("#excelButtonReportMohHd").on('click', function (e) {
    e.preventDefault();
    var form = $('#mohReportForm');
    form.attr('action', '/dnt/excel/generate-moh-report-hd');
    form.submit();
  });
  $("#excelButtonReportMohPd").on('click', function (e) {
    e.preventDefault();
    var form = $('#mohReportForm');
    form.attr('action', '/dnt/excel/generate-moh-report-pd');
    form.submit();
  });







  $("#excelButtonMohTime").on('click', function (e) {
    e.preventDefault();
    var form = $('#mohTimeFilterForm');
    form.attr('action', '/dnt/excel/generate-moh-time');
    form.submit();
  });

  $("#excelButtonMohDefaultTime").on('click', function (e) {
    e.preventDefault();
    var form = $('#mohTimeFilterForm');
    form.attr('action', '/dnt/filter/moh-time');
    form.submit();
  });


























});