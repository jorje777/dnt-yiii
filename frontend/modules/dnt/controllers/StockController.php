<?php

namespace frontend\modules\dnt\controllers;

use frontend\modules\dnt\helpers\GeneralHelper;
use frontend\modules\dnt\models\Drug;
use Yii;
use frontend\modules\dnt\models\Stock;
use frontend\modules\dnt\models\search\StockSearch;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * StockController implements the CRUD actions for Stock model.
 */
class StockController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => 'yii\filters\AccessControl',
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['manager'],
                        'denyCallback' => function () {
                            return Yii::$app->controller->redirect(['/dnt/patient/index']);
                        }
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all Stock models.
     * @return mixed
     */
    public function actionIndex($drug_type)
    {
        $searchModel = new StockSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams,$drug_type);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'drugType' => $drug_type,
            'sumPriceOfStock' => GeneralHelper::getStockSumPriceByDrugType($drug_type)
        ]);
    }

    /**
     * Displays a single Stock model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($drug_id,$drug_type)
    {
        $model = new Stock();
        $archive = Yii::$app->request->get('archive') ? 1 : 0;
        $drugModels = Stock::find()->notDeleted()->notSoldOut()->byDrugId($drug_id)->all();
        if($archive == 1){
            $drugModels = Stock::find()->notDeleted()->byDrugId($drug_id)->all();
        }
        return $this->render('view', [
            'drugModels' => $drugModels,
            'types' => Stock::getQuantityTypeConstants(),
            'drugId' => $drug_id,
            'archive' => $archive,
            'drugType' => $drug_type
        ]);
    }

    /**
     * Creates a new Stock model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Stock();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index','drug_type' => $model->drug->drug_type]);
        } else {
            $drugType = Yii::$app->request->get('drug_type');
            $drugs = Drug::find()->byDrugType($drugType)->all();
            return $this->render('create', [
                'model' => $model,
                'drugs' => ArrayHelper::map($drugs,'id','name'),
                'types' => Stock::getQuantityTypeConstants()
            ]);
        }
    }

    /**
     * Updates an existing Stock model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'drug_id' => $model->drug_id, 'drug_type' => $model->type]);
        }
    }

    /**
     * Deletes an existing Stock model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
//    public function actionDelete($id)
//    {
//        $this->findModel($id)->delete();
//
//        return $this->redirect(['index']);
//    }

    /**
     * Finds the Stock model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Stock the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Stock::find()->notDeleted()->byId($id)->one()) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('მოთხოვნილი გვერდი არ მოიძებნა.');
        }
    }
}
