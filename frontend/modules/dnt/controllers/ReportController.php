<?php
/**
 * Created by PhpStorm.
 * User: kalbievich
 * Date: 9/16/17
 * Time: 2:39 PM
 * @author Saiat Kalbiev <kalbievich11@gmail.com>
 */

namespace frontend\modules\dnt\controllers;


use DateTime;
use frontend\modules\dnt\helpers\GeneralHelper;
use frontend\modules\dnt\models\Expense;
use yii\web\Controller;

class ReportController extends Controller
{
    public function actionMoh(){
        $startDate = date("Y-m-d", strtotime("first day of this month"));
        $endDate = date("Y-m-d", strtotime("last day of this month"));
        $request = \Yii::$app->request->post();

        if(count($request) < 1){
            return $this->render('moh',[
                'startDate' => $startDate,
                'endDate' => $endDate,
                'expensesPd' => null,
                'expensesHd' => null
            ]);
        }

        $startDate = strtotime($request['start_date']);
        $endDate = strtotime($request['end_date']);

        $expensesHd = GeneralHelper::getReportForMohPrepsAndLabs($startDate,$endDate,"ჰ.დ");
        $expensesPd = GeneralHelper::getReportForMohPrepsAndLabs($startDate,$endDate,"პ.დ");

        if($expensesPd || $expensesHd){
            return $this->render('moh',[
                'startDate' => date("Y-m-d",$startDate),
                'endDate' => date("Y-m-d",$endDate),
                'expensesPd' => $expensesPd,
                'expensesHd' => $expensesHd
            ]);
        }else{
            return $this->render('moh',[
                'startDate' => date("Y-m-d",$startDate),
                'endDate' => date("Y-m-d",$endDate),
                'message' => "No Data Found",
                'expensesPd' => $expensesPd,
                'expensesHd' => $expensesHd
            ]);
        }
    }
}