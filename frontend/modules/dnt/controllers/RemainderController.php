<?php
/**
 * Created by PhpStorm.
 * User: sai
 * Date: 8/16/17
 * Time: 4:04 PM
 * @author Saiat Kalbiev <kalbievich11@gmail.com>
 */

namespace frontend\modules\dnt\controllers;


use frontend\modules\dnt\helpers\GeneralHelper;
use frontend\modules\dnt\models\Drug;
use frontend\modules\dnt\models\Remainder;
use frontend\modules\dnt\models\search\StockSearch;
use Yii;
use yii\db\Query;
use yii\web\Controller;

class RemainderController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => 'yii\filters\AccessControl',
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['manager'],
                        'denyCallback' => function () {
                            return Yii::$app->controller->redirect(['/dnt/patient/index']);
                        }
                    ],
                ],
            ],
        ];
    }
    public function actionIndex(){
        $request = Yii::$app->request->post();
        $stockData = GeneralHelper::getStockDrugSumByDrugType(0);

        if(count($request) < 1){
            return $this->render('index',[
                'stockData' => $stockData,
                'drugTypes' => Drug::getDrugTypes(),
                'type' => null,
                'message' => null
            ]);
        }
        $date = strtotime($request['date']);
        $drugType = $request['drug_type'];
        $stockData = GeneralHelper::getStockDrugSumByDrugType($drugType);

        $remainder = Remainder::find()->andWhere([
            'date' => $date,
            'type' => $drugType
        ])->one();

        if($remainder){
            return $this->render('index',[
                'stockData' => $stockData,
                'drugTypes' => Drug::getDrugTypes(),
                'type' => "danger",
                'message' => 'ინფომაცია არსებობს ბაზაში'
            ]);
        }


        $transaction = Yii::$app->db->beginTransaction();
        $check = true;
        foreach ($stockData as $data){
            $remainder = new Remainder();
            $remainder->date = $date;
            $remainder->drug_name = $data['name'];
            $remainder->quantity= floatval($data['quantity']);
            $remainder->stock_type = $data['type'];
            $remainder->sum_price = $data['sum_price'];
            $remainder->type = $drugType;
            if(!$remainder->save()){
                $transaction->rollBack();
                $check = false;
                break;
            }
        }
        if($check){
            $transaction->commit();
            return $this->render('index',[
                'stockData' => $stockData,
                'drugTypes' => Drug::getDrugTypes(),
                'type' => "success",
                'message' => 'წარმატებულია'

            ]);
        }
        return $this->render('index',[
            'stockData' => $stockData,
            'drugTypes' => Drug::getDrugTypes(),
            'type' => "danger",
            'message' => 'ვერ შევინახე'
        ]);

    }
    /**
     * @return \yii\console\Response|\yii\web\Response
     */
    public function actionGetStockByDrugType(){
        $drugType = Yii::$app->request->post('drug_type');
        $returnData = GeneralHelper::getStockDrugSumByDrugType($drugType);

        $response = Yii::$app->response;
        $response->format = \yii\web\Response::FORMAT_JSON;
        $response->data = $returnData;

        return $response;
    }
}