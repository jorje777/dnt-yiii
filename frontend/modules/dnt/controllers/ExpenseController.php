<?php
/**
 * Created by PhpStorm.
 * User: sai
 * Date: 8/9/17
 * Time: 11:51 AM
 * @author Saiat Kalbiev <kalbievich11@gmail.com>
 */

namespace frontend\modules\dnt\controllers;


use frontend\modules\dnt\helpers\GeneralHelper;
use frontend\modules\dnt\models\BloodLab;
use frontend\modules\dnt\models\Drug;
use frontend\modules\dnt\models\Expense;
use frontend\modules\dnt\models\Lab;
use frontend\modules\dnt\models\Patient;
use frontend\modules\dnt\models\Stock;
use Yii;
use yii\helpers\ArrayHelper;
use yii\web\Controller;

class ExpenseController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => 'yii\filters\AccessControl',
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['manager'],
                        'denyCallback' => function () {
                            return Yii::$app->controller->redirect(['/dnt/patient/index']);
                        }
                    ],
                ],
            ],
        ];
    }
    public function actionIndex(){
        $request = \Yii::$app->request->post();

        if(count($request) < 1){
            $drugs = Drug::find()->all();
            return $this->render('index',[
                'drugs' => ArrayHelper::map($drugs,'id','name'),
                'patients' => GeneralHelper::getPatients(),
                'message' => \Yii::$app->request->get('message') ? \Yii::$app->request->get('message') : null,
                'type' => \Yii::$app->request->get('type') ? \Yii::$app->request->get('type') : null
            ]);
        }

        $sessionDate = strtotime($request['session_date']);
        $transaction = \Yii::$app->db->beginTransaction();
        $patientId = $request['patient'];
        $comment = $request['comment'] == "" ? null : $request['comment'];

        //save lab
        if(isset($request['lab'])){
            foreach ($request['lab'] as $params){
                $lab = new Lab();
                if(!$lab->customLabSave($params,$sessionDate,$patientId)){
                    $transaction->rollBack();
                    return $this->redirect(['/dnt/expense/index','message' => 'ვერ მოხერხდა ლაბორატორიული კვლევის შენახვა']);
                }
            }
        }

        //save blood
        if(isset($request['bloodLab'])){
            foreach ($request['bloodLab'] as $params){
                $lab = new BloodLab();
                if(!$lab->customLabSave($params,$sessionDate,$patientId)){
                    $transaction->rollBack();
                    return $this->redirect(['/dnt/expense/index','message' => 'ვერ მოხერხდა სისძარღვოვანი მიდგომის შენახვა']);
                }
            }
        }

        //save expense
        if(isset($request['drugs'])){
            foreach ($request['drugs'] as $drugId => $quantity){
                $quantity = intval($quantity);
                if($quantity <= 0){
                    $transaction->rollBack();
                    return $this->redirect(['/dnt/expense/index','message' => 'რაოდენობა უნდა იყოს ერთი ან მეტი','type' => 'danger']);
                }
                $sum = Stock::find()->byDrugId($drugId)->notDeleted()->sum('quantity');
                if($sum < $quantity){
                    $transaction->rollBack();
                    return $this->redirect(['/dnt/expense/index','message' => 'პრეპარატის რაოდენობა არ არის ხელმისაწვდომი','type' => 'danger']);
                }

                $stocks = Stock::find()->byDrugId($drugId)->notDeleted()->orderBy('created_at ASC')->all();


                $nextQuantity = null;
                foreach ($stocks as $stock){
                    $expense = new Expense();
                    $expense->stock_id = $stock->id;
                    $expense->patient_id = $patientId;
                    $expense->comment = $comment;
                    $expense->session_date = $sessionDate;
                    $expense->created_at = time();

                    $stock->is_used = 1;
                    $stock->updated_at = time();


                    if($nextQuantity > 0){
                        $quantity = $nextQuantity;
                    }
                    if($stock->quantity === 0){
                        continue;
                    }

                    if($stock->quantity >= $quantity){
                        $stock->quantity = $stock->quantity - $quantity;
                        $expense->quantity = $quantity;
                        if(!$stock->save() || !$expense->save()){
                            $transaction->rollBack();
                            return $this->redirect([
                                '/dnt/expense/index',
                                'message' => 'ვერ მოხერხდა მარაგის ან დანახარჯის შენახვა',
                                'type' => 'danger'
                            ]);
                        }
                        break;
                    }   else {
                        $expense->quantity = $stock->quantity;
                        $nextQuantity = $quantity - $stock->quantity;
                        $stock->quantity = 0;
                        if(!$stock->save() || !$expense->save()){
                            $transaction->rollBack();
                            return $this->redirect([
                                '/dnt/expense/index',
                                'message' => 'ვერ მოხერხდა მარაგის ან დანახარჯის შენახვა',
                                'type' => 'danger'
                            ]);
                        }
                    }
                }
            }
        }








        $transaction->commit();
        return $this->redirect([
            '/dnt/expense/index',
            'message' => 'წარმატებულია',
            'type' => 'success'
        ]);

    }
}