<?php
/**
 * Created by PhpStorm.
 * User: kalbievich
 * Date: 8/13/17
 * Time: 12:51 PM
 */

namespace frontend\modules\dnt\controllers;


use centigen\base\helpers\QueryHelper;
use frontend\modules\dnt\helpers\GeneralHelper;
use frontend\modules\dnt\models\BloodLab;
use frontend\modules\dnt\models\Drug;
use frontend\modules\dnt\models\Expense;
use frontend\modules\dnt\models\Patient;
use frontend\modules\dnt\models\Protocol;
use frontend\modules\dnt\models\Remainder;
use frontend\modules\dnt\models\Stock;
use function GuzzleHttp\Psr7\str;
use Yii;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\web\Controller;

class FilterController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => 'yii\filters\AccessControl',
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['manager'],
                        'denyCallback' => function () {
                            return Yii::$app->controller->redirect(['/dnt/patient/index']);
                        }
                    ],
                ],
            ],
        ];
    }

    public function actionQuantity()
    {
        $request = Yii::$app->request->post();
        $startDate = date('Y-m-d', time() - 24 * 60 * 60);
        $endDate = date('Y-m-d', time() + 24 * 60 * 60);

        $drugsForFilter = Drug::find()->byDrugType(Drug::TYPE_STANDARD)->all();
        if (!$request) {
            return $this->render('quantity', [
                'drug_types' => Drug::getDrugTypes(),
                'drugs' => ArrayHelper::map($drugsForFilter, 'id', 'name'),
                'startDate' => $startDate,
                'endDate' => $endDate,
            ]);
        }

        $startDate = strtotime($request['start_date']);
        $endDate = strtotime($request['end_date']);
        $drugType = $request['drug_type'];
        $drugs = isset($request['drug']) ? $request['drug'] : null;

        $stocks = Stock::find();
        if ($drugs) {
            foreach ($drugs as $drug) {
                $stocks = $stocks->orWhere([
                    'drug_id' => $drug
                ]);
            }
        } else {
            $stocks = Stock::find()
                ->select('stock.* , drug.name as name')
                ->from('stock,drug')
                ->andWhere("stock.drug_id = drug.id")
                ->andWhere([
                    'drug.drug_type' => $drugType
                ]);
        }

        if ($startDate == $endDate) {
            $dateTimeStart = new \DateTime();
            $dateTimeEnd = new \DateTime();
            $dateTimeStart->setTimestamp($startDate);
            $dateTimeEnd->setTimestamp($startDate);
            $dateTimeStart->setTime(0, 0, 0);
            $dateTimeEnd->setTime(23, 59, 59);
            $startDate = $dateTimeStart->getTimestamp();
            $endDate = $dateTimeEnd->getTimestamp();
        }

        $stocks = $stocks
            ->andWhere("created_at > $startDate AND created_at < $endDate")
            ->with([
                'drug'
            ])->all();

        $expense = Expense::find()
            ->select('expense.*,stock.price,drug.name')
            ->from('expense,stock,drug')
            ->andWhere('stock.drug_id = drug.id')
            ->andWhere('expense.stock_id = stock.id');

        if ($drugs) {
            $stmt = "";
            $counter = 0;
            foreach ($drugs as $drug) {
                if ($counter == 0) {
                    $stmt .= " drug.id = $drug ";
                } else {
                    $stmt .= " OR drug.id = $drug ";
                }
                $counter++;
            }
            $expense->andWhere($stmt);
        } else {
            $expense = $expense->andWhere([
                'drug.drug_type' => $drugType
            ]);
        }
        $expense = $expense
            ->andWhere("session_date > $startDate AND session_date < $endDate")
            ->with(['stock', 'stock.drug'])->all();

        return $this->render('quantity', [
            'drug_types' => Drug::getDrugTypes(),
            'drugs' => ArrayHelper::map($drugsForFilter, 'id', 'name'),
            'startDate' => date('Y-m-d', $startDate),
            'endDate' => date('Y-m-d', $endDate),
            'in' => $stocks,
            'out' => $expense
        ]);

    }

    public function actionPrice()
    {
        $request = Yii::$app->request->post();
        $startDate = date('Y-m-d', time() - 24 * 60 * 60);
        $endDate = date('Y-m-d', time() + 24 * 60 * 60);

        $drugsForFilter = Drug::find()->byDrugType(Drug::TYPE_STANDARD)->all();
        if (!$request) {
            return $this->render('price', [
                'drug_types' => Drug::getDrugTypes(),
                'drugs' => ArrayHelper::map($drugsForFilter, 'id', 'name'),
                'startDate' => $startDate,
                'endDate' => $endDate,
            ]);
        }

        $startDate = strtotime($request['start_date']);
        $endDate = strtotime($request['end_date']);
        $drugType = $request['drug_type'];
        $drugs = isset($request['drug']) ? $request['drug'] : null;

        $stocks = Stock::find();
        if ($drugs) {
            foreach ($drugs as $drug) {
                $stocks = $stocks->orWhere([
                    'drug_id' => $drug
                ]);
            }
        } else {
            $stocks = Stock::find()
                ->select('stock.* , drug.name as name')
                ->from('stock,drug')
                ->andWhere("stock.drug_id = drug.id")
                ->andWhere([
                    'drug.drug_type' => $drugType
                ]);
        }
        $stocks = $stocks
            ->andWhere("stock.created_at > $startDate AND stock.created_at < $endDate")
            ->with(['drug'])
            ->all();


        $expense = Expense::find()
            ->select('expense.*,stock.price,drug.name')
            ->from('expense,stock,drug')
            ->andWhere('stock.drug_id = drug.id')
            ->andWhere('expense.stock_id = stock.id');

        if ($drugs) {
            $stmt = "";
            $counter = 0;
            foreach ($drugs as $drug) {
                if ($counter == 0) {
                    $stmt .= " drug.id = $drug ";
                } else {
                    $stmt .= " OR drug.id = $drug ";
                }
                $counter++;
            }
            $expense->andWhere($stmt);
        } else {
            $expense = $expense->andWhere([
                'drug.drug_type' => $drugType
            ]);
        }
        $expense = $expense
            ->andWhere("expense.session_date > $startDate AND expense.session_date < $endDate")
            ->with(['stock', 'stock.drug'])
            ->all();

        return $this->render('price', [
            'drug_types' => Drug::getDrugTypes(),
            'drugs' => ArrayHelper::map($drugsForFilter, 'id', 'name'),
            'startDate' => date('Y-m-d', $startDate),
            'endDate' => date('Y-m-d', $endDate),
            'in' => $stocks,
            'out' => $expense
        ]);
    }

    public function actionPatients()
    {
        $request = Yii::$app->request->post();
        $startDate = date('Y-m-d', time() - 24 * 60 * 60);
        $endDate = date('Y-m-d', time() + 24 * 60 * 60);
        $selectedDrugType = ArrayHelper::getValue($request, 'drug_type', null);

        $drugsForFilter = Drug::find()->byDrugType(Drug::TYPE_STANDARD)->all();

        if (!$request) {
            return $this->render('patients', [
                'drug_types' => Drug::getDrugTypes(),
                'drugs' => ArrayHelper::map($drugsForFilter, 'id', 'name'),
                'startDate' => $startDate,
                'endDate' => $endDate,
                'patients' => GeneralHelper::getPatients(),
                'patientGroups' => Patient::GROUPS,
                'selectedPatient' => null,
                'selectedGroups' => null,
                'selectedDrugType' => $selectedDrugType,
                'bloodLabs' => null,
                'labs' => null,
            ]);
        }
        $startDate = strtotime($request['start_date']);
        $endDate = strtotime($request['end_date']);

        $patientId = $request['patient'] == "" ? null : $request['patient'];
        $patientGroups = isset($request['patientGroups']) ? $request['patientGroups'] : null;

        $expense = GeneralHelper::getExpenseByPatientAndGroups($patientId, $patientGroups, $startDate, $endDate, $selectedDrugType);
        $bloodLabs = GeneralHelper::getBloodLab($startDate, $endDate, $patientId, $patientGroups);
        $labs = GeneralHelper::getLab($startDate, $endDate, $patientId, $patientGroups);

        $sumPrice = 0;
        /** @var Expense $exp */
        foreach ($expense as $exp) {
            $sumPrice += $exp->quantity * $exp->stock->price;
        }

        $selectedGroups = [];
        if (count($patientGroups) > 0) {
            foreach ($patientGroups as $key => $value) {
                $selectedGroups[$value] = $value;
            }
        }
        return $this->render('patients', [
            'drug_types' => Drug::getDrugTypes(),
            'drugs' => ArrayHelper::map($drugsForFilter, 'id', 'name'),
            'startDate' => date('Y-m-d', $startDate),
            'endDate' => date('Y-m-d', $endDate),
            'patients' => GeneralHelper::getPatients(),
            'patientGroups' => Patient::GROUPS,
            'out' => $expense,
            'selectedPatient' => $patientId,
            'selectedGroups' => $selectedGroups,
            'sumPrice' => $sumPrice,
            'selectedDrugType' => $selectedDrugType,
            'bloodLabs' => $bloodLabs,
            'labs' => $labs,
        ]);
    }

    public function actionProtocols()
    {
        $request = Yii::$app->request->post();
        $startDate = date('Y-m-d', time() - 24 * 60 * 60);
        $endDate = date('Y-m-d', time() + 24 * 60 * 60);
        $selectedProtocol = ArrayHelper::getValue($request, 'protocol_type', null);

        if (!$request) {
            return $this->render('protocols', [
                'protocolTypes' => Protocol::getProtocolTypes(),
                'startDate' => $startDate,
                'endDate' => $endDate,
                'selectedProtocol' => $selectedProtocol
            ]);
        }

        $startDate = strtotime($request['start_date']);
        $endDate = strtotime($request['end_date']);

        $protocols = GeneralHelper::getProtocolsBySessionDateGroupedBySessionDate($startDate, $endDate, $selectedProtocol);

        return $this->render('protocols', [
            'protocolTypes' => Protocol::getProtocolTypes(),
            'startDate' => date('Y-m-d', $startDate),
            'endDate' => date('Y-m-d', $endDate),
            'protocols' => $protocols,
            'selectedProtocol' => $selectedProtocol

        ]);
    }

    public function actionRemainder()
    {
        $request = Yii::$app->request->post();

        if (count($request) < 1) {
            return $this->render('remainder', [
                'drugTypes' => Drug::getDrugTypes(),
                'type' => null,
                'message' => null
            ]);
        }
        $date = strtotime($request['date']);
        $drugType = $request['drug_type'];
        $remainders = Remainder::find()->andWhere([
            'type' => $drugType,
            'date' => $date
        ])->all();
        if ($remainders) {
            return $this->render('remainder', [
                'drugTypes' => Drug::getDrugTypes(),
                'remainders' => $remainders,
                'type' => null,
                'message' => null,
                'drugType' => $drugType,
                'selectedDate' => $request['date']

            ]);
        }
        return $this->render('remainder', [
            'drugTypes' => Drug::getDrugTypes(),
            'type' => "danger",
            'selectedDate' => $request['date'],
            'drugType' => $drugType,
            'message' => "ინფორმაცია არ მოიძებნა"
        ]);

    }

    public function actionMoh()
    {
        $startDate = date('Y-m-d', time() - 60 * 60 * 24);
        $endDate = date('Y-m-d', time() + 60 * 60 * 24);
        $request = Yii::$app->request->post();

        if (count($request) < 1) {
            return $this->render('moh', [
                'patients' => GeneralHelper::getPatients(),
                'drug_types' => Drug::getDrugTypes(),
                'startDate' => $startDate,
                'endDate' => $endDate,
                'selectedPatient' => null,
                'selectedDrug' => null
            ]);
        }

        $startDate = strtotime($request['start_date']);
        $endDate = strtotime($request['end_date']);
        $patientId = $request['patient_id'] !== "" ? $request['patient_id'] : null;
        $drug_type = $request['drug_type'];

        $pivotedArrays = GeneralHelper::getExpensesBySessionDatePivoted($startDate, $endDate, $patientId, $drug_type);

        if (!$pivotedArrays || count($pivotedArrays) < 1) {
            $startDate = date("Y-m-d", $startDate);
            $endDate = date("Y-m-d", $endDate);
            return $this->render('moh', [
                'patients' => GeneralHelper::getPatients(),
                'drug_types' => Drug::getDrugTypes(),
                'startDate' => $startDate,
                'endDate' => $endDate,
                'selectedPatient' => $patientId,
                'selectedDrug' => $drug_type
            ]);
        }

        return $this->render('moh', [
            'patients' => GeneralHelper::getPatients(),
            'startDate' => date('Y-m-d', $startDate),
            'drug_types' => Drug::getDrugTypes(),
            'endDate' => date('Y-m-d', $endDate),
            'expenseData' => $pivotedArrays['expenseData'],
            'drugs' => $pivotedArrays['drugs'],
            'selectedPatient' => $patientId,
            'selectedDrug' => $drug_type
        ]);
    }


    public function actionMohTime()
    {
        $date = date('Y-m-d', time() - 60 * 60 * 24);
        $startTime = "09:00";
        $endTime = "21:00";
        $request = Yii::$app->request->post();

        if (count($request) < 1) {
            return $this->render('moh_time', [
                'patients' => GeneralHelper::getPatients(),
                'drug_types' => Drug::getDrugTypes(),
                'date' => $date,
                'startTime' => $startTime,
                'endTime' => $endTime,
                'selectedPatient' => null,
                'selectedDrug' => null
            ]);
        }

        $date = strtotime($request['date']);
        $startTime = ArrayHelper::getValue($request, 'startTime', null);
        $endTime = ArrayHelper::getValue($request, 'endTime', null);
        $patientId = $request['patient_id'] !== "" ? $request['patient_id'] : null;
        $drug_type = $request['drug_type'];

        $pivotedArrays = GeneralHelper::getExpensesBySessionDateAndTimePivoted($date, $startTime, $endTime, $patientId, $drug_type);

        if (!$pivotedArrays || count($pivotedArrays) < 1) {
            $date = date("Y-m-d", $date);
            return $this->render('moh_time', [
                'patients' => GeneralHelper::getPatients(),
                'drug_types' => Drug::getDrugTypes(),
                'date' => $date,
                'startTime' => $startTime,
                'endTime' => $endTime,
                'selectedPatient' => $patientId,
                'selectedDrug' => $drug_type
            ]);
        }
        return $this->render('moh_time', [
            'patients' => GeneralHelper::getPatients(),
            'date' => date('Y-m-d', $date),
            'startTime' => $startTime,
            'endTime' => $endTime,
            'drug_types' => Drug::getDrugTypes(),
            'expenseData' => $pivotedArrays['expenseData'],
            'drugs' => $pivotedArrays['drugs'],
            'selectedPatient' => $patientId,
            'selectedDrug' => $drug_type
        ]);
    }


    /**
     * @return \yii\console\Response|\yii\web\Response
     */
    public function actionGetDrugsByType()
    {
        $drugTypeId = Yii::$app->request->post('typeId');
        $returnData = Drug::find()->byDrugType($drugTypeId)->all();

        $response = Yii::$app->response;
        $response->format = \yii\web\Response::FORMAT_JSON;
        $response->data = $returnData;

        return $response;
    }

}