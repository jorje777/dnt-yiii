<?php
/**
 * Created by PhpStorm.
 * User: sai
 * Date: 8/15/17
 * Time: 6:07 PM
 * @author Saiat Kalbiev <kalbievich11@gmail.com>
 */

namespace frontend\modules\dnt\controllers;


use centigen\base\helpers\DateHelper;
use frontend\modules\dnt\helpers\GeneralHelper;
use frontend\modules\dnt\models\Expense;
use frontend\modules\dnt\models\Remainder;
use frontend\modules\dnt\models\Stock;
use PHPExcel;
use PHPExcel_IOFactory;
use Yii;
use yii\helpers\ArrayHelper;
use yii\web\Controller;

class ExcelController extends Controller
{
    public function actionGeneratePatientsExcel(){
        $request = \Yii::$app->request->post();
        $startDate = strtotime($request['start_date']);
        $endDate = strtotime($request['end_date']);
        $selectedDrugType = ArrayHelper::getValue($request,'drug_type',null);

        $patientId = $request['patient'] == "" ? null : $request['patient'];
        $patientGroups = isset($request['patientGroups']) ? $request['patientGroups'] : null;

        $expense = GeneralHelper::getExpenseByPatientAndGroups($patientId,$patientGroups,$startDate,$endDate,$selectedDrugType);

        if(count($expense) < 0){
            return false;
        }
        $sumPrice = 0;
        /** @var Expense $exp */
        foreach ($expense as $exp){
            $sumPrice += $exp->quantity * $exp->stock->price;
        }

        $excel = new PHPExcel();
        $sheet = $excel->getActiveSheet();

        for($i = 0;$i < 8;$i++){
            $sheet->getColumnDimension(range('A','Z')[$i])->setAutoSize(true);
        }
        $sheet->setCellValueByColumnAndRow(6,1,$sumPrice);
        $sheet->setCellValueByColumnAndRow(0,2,'თარიღი');
        $sheet->setCellValueByColumnAndRow(1,2,'გვარი, სახელი');
        $sheet->setCellValueByColumnAndRow(2,2,'პრეპარატი');
        $sheet->setCellValueByColumnAndRow(3,2,'რაოდენობა');
        $sheet->setCellValueByColumnAndRow(4,2,'ტიპი');
        $sheet->setCellValueByColumnAndRow(5,2,'ერთეულის ფასი');
        $sheet->setCellValueByColumnAndRow(6,2,'საერთო ფასი');
        $sheet->setCellValueByColumnAndRow(7,2,'კომენტარი');

        $row = 3;
        /** @var Expense $exp */
        foreach ($expense as $exp){
            $sheet->setCellValueByColumnAndRow(0,$row,date("Y-m-d",$exp->session_date));
            $sheet->setCellValueByColumnAndRow(1,$row,$exp->patient->getFullName());
            $sheet->setCellValueByColumnAndRow(2,$row,$exp->stock->drug->name);
            $sheet->setCellValueByColumnAndRow(3,$row,$exp->quantity);
            $sheet->setCellValueByColumnAndRow(4,$row,$exp->stock->type ? Stock::getQuantityTypeConstants()[$exp->stock->type] : "");
            $sheet->setCellValueByColumnAndRow(5,$row,$exp->stock->price);
            $sheet->setCellValueByColumnAndRow(6,$row,number_format($exp->stock->price * $exp->quantity,7));
            $sheet->setCellValueByColumnAndRow(7,$row,$exp->comment);
            $row++;
        }

        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="Patients ' . DateHelper::getNowIntoFormat('Y-MM-d') .'.xlsx"');
        header('Cache-Control: max-age=0');

        $objWriter = PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
        $objWriter->save('php://output');
    }

    public function actionGenerateProtocolsExcel(){
        $request = \Yii::$app->request->post();
        $startDate = strtotime($request['start_date']);
        $endDate = strtotime($request['end_date']);
        $selectedProtocol = ArrayHelper::getValue($request,'protocol_type',null);

        $protocols = GeneralHelper::getProtocolsBySessionDateGroupedBySessionDate($startDate,$endDate,$selectedProtocol);

        if(count($protocols) < 0){
            return false;
        }


        $excel = new PHPExcel();
        $sheet = $excel->getActiveSheet();

        for($i = 0;$i < 5;$i++){
            $sheet->getColumnDimension(range('A','Z')[$i])->setAutoSize(true);
        }

        $sheet->setCellValueByColumnAndRow(0,2,'სეანსის თარიღი');
        $sheet->setCellValueByColumnAndRow(1,2,'გვარი, სახელი');
        $sheet->setCellValueByColumnAndRow(2,2,'რაოდენობა');

        $row = 3;
        $sum = 0;
        foreach ($protocols as $protocol){
            $sheet->setCellValueByColumnAndRow(0,$row,date("Y-m-d",$protocol['session_date']));
            $sheet->setCellValueByColumnAndRow(1,$row, $protocol['surname']. " ". $protocol["name"]);
            $sheet->setCellValueByColumnAndRow(2,$row,$protocol['quantity']);
            $row++;
            $sum += $protocol['quantity'];
        }
        $sheet->setCellValueByColumnAndRow(2,1,'ჯამი '.$sum);

        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="Protocols ' . DateHelper::getNowIntoFormat('Y-MM-d') .'.xlsx"');
        header('Cache-Control: max-age=0');

        $objWriter = PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
        $objWriter->save('php://output');
    }

    public function actionGenerateRemainderExcel(){
        $request = Yii::$app->request->post();
        if(count($request) < 0){
            return false;
        }
        $date = strtotime($request['date']);
        $drugType = $request['drug_type'];
        $remainders = Remainder::find()->andWhere([
            'type' => $drugType,
            'date' => $date
        ])->all();

        $excel = new PHPExcel();
        $sheet = $excel->getActiveSheet();

        for($i = 0;$i < 5;$i++){
            $sheet->getColumnDimension(range('A','Z')[$i])->setAutoSize(true);
        }

        $sheet->setCellValueByColumnAndRow(0,1,'პრეპარატი');
        $sheet->setCellValueByColumnAndRow(1,1,'რაოდენობა');
        $sheet->setCellValueByColumnAndRow(2,1,'ტიპი');

        $row = 2;
        $sum = 0;
        foreach ($remainders as $remainder){
            $sheet->setCellValueByColumnAndRow(0,$row,$remainder->drug_name);
            $sheet->setCellValueByColumnAndRow(1,$row,$remainder->quantity);
            $sheet->setCellValueByColumnAndRow(2,$row,$remainder->stock_type ? Stock::getQuantityTypeConstants()[$remainder->stock_type] : "");
            $sum += $remainder->sum_price;
            $row++;
        }
        $sheet->setCellValueByColumnAndRow(1,$row,"ჯამი : " . $sum);

        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="Remainder ' . DateHelper::getNowIntoFormat('Y-MM-d') .'.xlsx"');
        header('Cache-Control: max-age=0');

        $objWriter = PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
        $objWriter->save('php://output');

    }

    public function actionGenerateMoh()
    {
        $request = Yii::$app->request->post();
        if (count($request) < 0) {
            return false;
        }

        $startDate = strtotime($request['start_date']);
        $endDate = strtotime($request['end_date']);
        $patientId = ArrayHelper::getValue($request,'patient_id',null);
        $drugType = ArrayHelper::getValue($request,'drug_type',null);
        $expenseIds = ArrayHelper::getValue($request,'expense_ids',null);

        $pivotedArrays = GeneralHelper::getExpensesBySessionDatePivoted($startDate, $endDate,$patientId,$drugType);

        if (!$pivotedArrays || count($pivotedArrays) < 1) {
            return false;
        }
        $excel = new PHPExcel();
        $sheet = $excel->getActiveSheet();

        $drugs = $pivotedArrays['drugs'];
        $expenseData = $pivotedArrays['expenseData'];

        for($i = 0;$i <= count($drugs);$i++){
            $sheet->getColumnDimensionByColumn($i)->setAutoSize(true);
        }

        $sheet->setCellValueByColumnAndRow(0,1,'პრეპარატი');
        $sheet->getRowDimension(1)->setRowHeight(100);

        $column = 1;
        foreach ($drugs as $drug){
            $sheet->setCellValueByColumnAndRow($column,1,$drug);
            $sheet->getCellByColumnAndRow($column,1)->getStyle()->getAlignment()->setTextRotation(90);
            $column++;
        }

        $row = 2;
        if($expenseIds){
            foreach ($expenseData as $patient){
                if(in_array($patient['id'],$expenseIds)){
                    $sheet->setCellValueByColumnAndRow(0,$row,$patient['name']);
                    $column = 1;
                    foreach ($patient['drugs'] as $drugCount) {
                        $sheet->setCellValueByColumnAndRow($column,$row,$drugCount);
                        $column++;
                    }
                    $row++;
                }
            }
        }else{
            foreach ($expenseData as $patient){
                $sheet->setCellValueByColumnAndRow(0,$row,$patient['name']);
                $column = 1;
                foreach ($patient['drugs'] as $drugCount) {
                    $sheet->setCellValueByColumnAndRow($column,$row,$drugCount);
                    $column++;
                }
                $row++;
            }
        }

        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="MOH ' . DateHelper::getNowIntoFormat('Y-MM-d') .'.xlsx"');
        header('Cache-Control: max-age=0');
        $objWriter = PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
        $objWriter->save('php://output');

    }

    public function actionGenerateMohReportPd(){
        $request = Yii::$app->request->post();

        if(count($request) < 1){
            return false;
        }

        $startDate = strtotime($request['start_date']);
        $endDate = strtotime($request['end_date']);

        return $this->generateMohReportByGroup($startDate,$endDate,"პ.დ");
    }

    public function actionGenerateMohReportHd(){
        $request = Yii::$app->request->post();

        if(count($request) < 1){
            return false;
        }

        $startDate = strtotime($request['start_date']);
        $endDate = strtotime($request['end_date']);

        return $this->generateMohReportByGroup($startDate,$endDate,"ჰ.დ");
    }

    private function generateMohReportByGroup($startDate,$endDate,$group){
        $expenses = GeneralHelper::getReportForMohPrepsAndLabs($startDate,$endDate,$group);

        if(count($expenses) < 1){
            return false;
        }

        $excel = new PHPExcel();
        $sheet = $excel->getActiveSheet();
        for($i = 0;$i < 3;$i++){
            $sheet->getColumnDimension(range('A','Z')[$i])->setAutoSize(true);
        }
        $sheet->setCellValueByColumnAndRow(0,1,'სახელი');
        $sheet->setCellValueByColumnAndRow(1,1,'პრეპ. რაოდენობა');
        $sheet->setCellValueByColumnAndRow(2,1,'ლაბ');

        $row = 2;
        foreach ($expenses as $expense){
            $sheet->setCellValueByColumnAndRow(0,$row,$expense['name']);
            $sheet->setCellValueByColumnAndRow(1,$row,$expense['prep']);
            $sheet->setCellValueByColumnAndRow(2,$row,$expense['lab']);
            $row++;
        }

        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="MOH ' . DateHelper::getNowIntoFormat('Y-MM-d') .'.xlsx"');
        header('Cache-Control: max-age=0');
        $objWriter = PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
        $objWriter->save('php://output');
    }




    public function actionGenerateMohTime()
    {
        $request = Yii::$app->request->post();
        if (count($request) < 0) {
            return false;
        }
        $date = strtotime($request['date']);
        $startTime = ArrayHelper::getValue($request,'startTime',null);
        $endTime = ArrayHelper::getValue($request,'endTime',null);
        $patientId = ArrayHelper::getValue($request,'patient_id',null);
        $drugType = ArrayHelper::getValue($request,'drug_type',null);
        $expenseIds = ArrayHelper::getValue($request,'expense_ids',null);

        $pivotedArrays = GeneralHelper::getExpensesBySessionDateAndTimePivoted($date,$startTime,$endTime,$patientId,$drugType);

        if (!$pivotedArrays || count($pivotedArrays) < 1) {
            return false;
        }

        $excel = new PHPExcel();
        $sheet = $excel->getActiveSheet();

        $drugs = $pivotedArrays['drugs'];
        $expenseData = $pivotedArrays['expenseData'];

        for($i = 0;$i <= count($drugs);$i++){
            $sheet->getColumnDimensionByColumn($i)->setAutoSize(true);
        }

        $sheet->setCellValueByColumnAndRow(0,1,'პრეპარატი');
        $sheet->getRowDimension(1)->setRowHeight(100);

        $column = 1;
        foreach ($drugs as $drug){
            $sheet->setCellValueByColumnAndRow($column,1,$drug);
            $sheet->getCellByColumnAndRow($column,1)->getStyle()->getAlignment()->setTextRotation(90);
            $column++;
        }

        $row = 2;
        if($expenseIds){
            foreach ($expenseData as $patient){
                if(in_array($patient['id'],$expenseIds)){
                    $sheet->setCellValueByColumnAndRow(0,$row,$patient['name']);
                    $column = 1;
                    foreach ($patient['drugs'] as $drugCount) {
                        $sheet->setCellValueByColumnAndRow($column,$row,$drugCount);
                        $column++;
                    }
                    $row++;
                }
            }
        }else{
            foreach ($expenseData as $patient){
                $sheet->setCellValueByColumnAndRow(0,$row,$patient['name']);
                $column = 1;
                foreach ($patient['drugs'] as $drugCount) {
                    $sheet->setCellValueByColumnAndRow($column,$row,$drugCount);
                    $column++;
                }
                $row++;
            }
        }

        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="MOH ' . DateHelper::getNowIntoFormat('Y-MM-d') .'.xlsx"');
        header('Cache-Control: max-age=0');
        $objWriter = PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
        $objWriter->save('php://output');

    }
}