<?php

namespace frontend\modules\dnt\controllers;

use frontend\modules\dnt\helpers\GeneralHelper;
use frontend\modules\dnt\models\Drug;
use frontend\modules\dnt\models\Expense;
use frontend\modules\dnt\models\Patient;
use frontend\modules\dnt\models\Stock;
use Yii;
use frontend\modules\dnt\models\Protocol;
use frontend\modules\dnt\models\search\ProtocolSearch;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ProtocolController implements the CRUD actions for Protocol model.
 */
class ProtocolController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
//            'verbs' => [
//                'class' => VerbFilter::className(),
//                'actions' => [
//                    'delete' => ['POST'],
//                ],
//            ],
            'access' => [
                'class' => 'yii\filters\AccessControl',
                'rules' => [
                    [

                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all Protocol models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ProtocolSearch();
        $type = Yii::$app->request->get('type');

        $startDateFromGet = Yii::$app->request->get('start_date');
        $endDateFromGet = Yii::$app->request->get('end_date');

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams,false);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'startDate' => $startDateFromGet ? $startDateFromGet : null,
            'endDate' => $endDateFromGet ? $endDateFromGet : null,
            'type' => $type,
            'patients' => GeneralHelper::getPatients(),
            'patient_types' => array_merge(['' => ''],Patient::GROUPS)
        ]);
    }

    public function actionViewPerPatient(){
        $searchModel = new ProtocolSearch();
        $type = Yii::$app->request->get('type');
        $patientId = Yii::$app->request->get('patient_id');

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams,true,$patientId);

        return $this->render('index-per-patient', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'type' => $type,
        ]);
    }
    /**
     * Displays a single Protocol model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Protocol model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $protocol = new Protocol();
        $type = Yii::$app->request->get('type');
        $request = Yii::$app->request->post();
        $drugs = GeneralHelper::getDrugsColumnedArrayByDrugType($type);

        if(count($request) < 1){
            return $this->render('create', [
                'patients' => GeneralHelper::getPatients(),
                'model' => $protocol,
                'type' => $type,
                'drugs' => $drugs,
                'message' => \Yii::$app->request->get('message') ? \Yii::$app->request->get('message') : null,
                'messageType' => \Yii::$app->request->get('messageType') ? \Yii::$app->request->get('messageType') : null
            ]);
        }
        $response = GeneralHelper::saveProtocolWithExpense($protocol,$request);

        if($response['success']){
            return $this->redirect(['index','type' => $type]);
        }
        return $this->render('create',[
            'message' => $response['message'],
            'messageType' => 'danger',
            'type' => $type,
            'patients' => GeneralHelper::getPatients(),
            'model' => $protocol,
            'drugs' => $drugs,
        ]);

    }

    /**
     * Updates an existing Protocol model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $protocol = $this->findModel($id);
        $type = Yii::$app->request->get('type');
        $request = Yii::$app->request->post();
        $drugs = GeneralHelper::getDrugsColumnedArrayByDrugType($type);

        $expenseDrugs = Expense::find()
            ->select('expense.*,stock.price,drug.name')
            ->from('expense,stock,drug')
            ->andWhere(['expense.protocol_id' => $id])
            ->andWhere('stock.drug_id = drug.id')
            ->andWhere('expense.stock_id = stock.id')
            ->with(['stock','stock.drug'])
            ->all();

        if(count($request) < 1){
            return $this->render('update', [
                'expenseDrugs' => $expenseDrugs,
                'patients' => GeneralHelper::getPatients(),
                'model' => $protocol,
                'type' => $type,
                'drugs' => $drugs,
                'message' => \Yii::$app->request->get('message') ? \Yii::$app->request->get('message') : null,
                'messageType' => \Yii::$app->request->get('messageType') ? \Yii::$app->request->get('messageType') : null
            ]);
        }



        $response = GeneralHelper::saveProtocolWithExpense($protocol,$request);

        if($response['success']){
            return $this->redirect(['index','type' => $type]);
        }
        return $this->render('update',[
            'expenseDrugs' => $expenseDrugs,
            'message' => $response['message'],
            'messageType' => 'danger',
            'type' => $type,
            'patients' => GeneralHelper::getPatients(),
            'model' => $protocol,
            'drugs' => $drugs,
        ]);
    }

    /**
     * Deletes an existing Protocol model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id,$type)
    {
        $protocol = Protocol::find()->byId($id)->one();
        if($protocol){
            $protocol->markDeleted();
        }
        return $this->redirect(['index','type' => $type]);
    }

    /**
     * Finds the Protocol model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Protocol the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Protocol::find()->byId($id)->notDeleted()->one()) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('მოთხოვნილი გვერდი არ მოიძებნა.');
        }
    }

    /**
     * @return array|\yii\console\Response|\yii\web\Response
     * @author Saiat Kalbiev <kalbievich11@gmail.com>
     */
    public function actionCheckDrugQuantity(){
        $request = Yii::$app->request->post();

        if(count($request) < 1){
            return [
                'success' => false
            ];
        }

        $returnData = [
            'success' => true,
            'data' => []
        ];

        foreach ($request['drugs'] as $drug){
            $sum = Stock::find()->byDrugId($drug['drug_id'])->notDeleted()->sum('quantity');
            $dr = Drug::find()->byId($drug['drug_id'])->one();

            $isEnough =true;
            if ($sum < $drug['quantity']) {
                $returnData['success'] = false;
                $isEnough = false;
            }
            array_push($returnData['data'],[
                'name' => $dr->name,
                'quantity' => $drug['quantity'],
                'stock' => $sum,
                'is_enough' => $isEnough
            ]);
        }

        $response = Yii::$app->response;
        $response->format = \yii\web\Response::FORMAT_JSON;
        $response->data = $returnData;
        return $response;
    }
}
