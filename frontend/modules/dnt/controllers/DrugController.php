<?php

namespace frontend\modules\dnt\controllers;

use frontend\modules\dnt\models\DrugCategory;
use Yii;
use frontend\modules\dnt\models\Drug;
use frontend\modules\dnt\models\search\DrugSearch;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * DrugController implements the CRUD actions for Drug model.
 */
class DrugController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => 'yii\filters\AccessControl',
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['manager'],
                        'denyCallback' => function () {
                            return Yii::$app->controller->redirect(['/dnt/patient/index']);
                        }
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all Drug models.
     * @return mixed
     */
    public function actionIndex()
    {
        $model = new Drug();
        $drugType = Yii::$app->request->get('drug_type');
        $drugCategories = null;
        if($drugType == "1"){
            $drugCategories = DrugCategory::find()->byMwvave(false)->all();
            $drugCategories = ArrayHelper::map($drugCategories, 'id', 'name');
        }else if($drugType == "2"){
            $drugCategories = DrugCategory::find()->byMwvave(true)->all();
            $drugCategories = ArrayHelper::map($drugCategories, 'id', 'name');
        }


        $searchModel = new DrugSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        if(count(Yii::$app->request->post()) < 1){
            return $this->render('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
                'drugTypes' => Drug::getDrugTypes(),
                'model' => $model,
                'drugType' => $drugType,
                'drugCategories' => $drugCategories,
                'type' => null,
                'message' => null
            ]);
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $model = new Drug();
            return $this->render('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
                'drugTypes' => Drug::getDrugTypes(),
                'model' => $model,
                'drugType' => $drugType,
                'drugCategories' => $drugCategories,
                'type' => 'success',
                'message' => 'წარმატებულია'
            ]);
        }
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'drugTypes' => Drug::getDrugTypes(),
            'model' => $model,
            'drugType' => $drugType,
            'drugCategories' => $drugCategories,
            'type' => 'danger',
            'message' => 'შეცდომა'
        ]);


    }

    /**
     * Displays a single Drug model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Drug model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
//    public function actionCreate()
//    {
//        $model = new Drug();
//
//        if ($model->load(Yii::$app->request->post()) && $model->save()) {
//            return $this->redirect(['index','drug_type' => $model->drug_type]);
//        }
//    }

    /**
     * Updates an existing Drug model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index','drug_type' => $model->drug_type]);
        } else {
            $drugType = $model->drug_type;
            $drugCategories = null;
            if($drugType == "1"){
                $drugCategories = DrugCategory::find()->byMwvave(false)->all();
                $drugCategories = ArrayHelper::map($drugCategories, 'id', 'name');
            }else if($drugType == "2"){
                $drugCategories = DrugCategory::find()->byMwvave(true)->all();
                $drugCategories = ArrayHelper::map($drugCategories, 'id', 'name');
            }

            return $this->render('update', [
                'model' => $model,
                'drugType' => $model->drug_type,
                'drugCategories' => $drugCategories
            ]);
        }
    }

    /**
     * Deletes an existing Drug model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $drug = $this->findModel($id);
        $drug_type = $drug->drug_type;
        $drug->delete();

        return $this->redirect(['index','drug_type' => $drug_type]);
    }

    /**
     * Finds the Drug model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Drug the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Drug::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('მოთხოვნილი გვერდი არ მოიძებნა.');
        }
    }
}
