<?php

namespace frontend\modules\dnt\helpers;

use centigen\base\helpers\QueryHelper;
use frontend\modules\dnt\models\BloodLab;
use frontend\modules\dnt\models\Drug;
use frontend\modules\dnt\models\DrugCategory;
use frontend\modules\dnt\models\Expense;
use frontend\modules\dnt\models\Lab;
use frontend\modules\dnt\models\Patient;
use frontend\modules\dnt\models\Protocol;
use frontend\modules\dnt\models\Stock;
use Yii;
use yii\db\Query;

/**
 * Created by PhpStorm.
 * User: sai
 * Date: 8/9/17
 * Time: 12:20 PM
 * @author Saiat Kalbiev <kalbievich11@gmail.com>
 */
class GeneralHelper
{
    /**
     * @return array
     * @author Saiat Kalbiev <kalbievich11@gmail.com>
     */
    public static function getPatients()
    {
        $patientsObjects = Patient::find()->notDeleted()->all();
        $patients = [];
        foreach ($patientsObjects as $patient) {
            $patients[$patient->id] = $patient->getFullName() . " - " . $patient->personal_id;
        }
        return $patients;
    }

    /**
     * @param $patientId
     * @param $patientGroups
     * @param $startDate
     * @param $endDate
     * @return array|Expense[]
     */
    public static function getExpenseByPatientAndGroups($patientId, $patientGroups, $startDate, $endDate, $drugType)
    {
        $expense = Expense::find()
            ->select('expense.*,stock.price,drug.name')
            ->from('expense,stock,drug,patient')
            ->andWhere('stock.drug_id = drug.id')
            ->andWhere('expense.stock_id = stock.id')
            ->andWhere('expense.patient_id = patient.id');

        if ($patientId) {
            $expense = $expense->andWhere([
                'expense.patient_id' => $patientId
            ]);
        }

        if ($drugType || $drugType == "0") {

            $expense = $expense->andWhere([
                'drug.drug_type' => $drugType
            ]);
        }
        if ($patientGroups) {
            $stmt = "";
            $counter = 0;
            foreach ($patientGroups as $group) {
                if ($counter == 0) {
                    $stmt .= " patient.group = '$group' ";
                } else {
                    $stmt .= " OR patient.group = '$group' ";
                }
                $counter++;
            }
            $expense = $expense->andWhere($stmt);
        }


        return $expense
            ->andWhere("expense.session_date > $startDate AND expense.session_date < $endDate")
            ->with(['stock', 'stock.drug', 'patient'])->all();
    }

    /**
     * @param $startDate
     * @param $endDate
     * @return array
     * @author Saiat Kalbiev <kalbievich11@gmail.com>
     */
    public static function getProtocolsBySessionDateGroupedBySessionDate($startDate, $endDate, $protocolType)
    {
        return (new Query())
            ->select("pa.id,pr.id,pr.session_date,pa.name as name,pa.surname as surname,count(pr.id) as quantity")
            ->from('patient pa')
            ->join('inner join', 'protocol pr', 'pa.id = pr.patient_id')
            ->groupBy('pa.id,pr.session_date')
            ->andWhere("pr.session_date > $startDate and pr.session_date < $endDate")
            ->andWhere("pr.type = $protocolType")
            ->andWhere('pr.deleted_at IS NULL')
            ->all();
    }

    /**
     * @param $drug_type
     * @return array|bool
     * @author Saiat Kalbiev <kalbievich11@gmail.com>
     */
    public static function getStockSumPriceByDrugType($drug_type)
    {
        return (new Query())
            ->select('sum(st.price * st.quantity) as sum')
            ->from('stock st, drug dr')
            ->andWhere("st.drug_id = dr.id and dr.drug_type = $drug_type")
            ->one();
    }

    /**
     * @param $drugType
     * @return array
     * @author Saiat Kalbiev <kalbievich11@gmail.com>
     */
    public static function getStockDrugSumByDrugType($drugType)
    {

        return (new Query())
            ->select('st.type,dr.name,SUM(st.quantity) as quantity,SUM(st.quantity * st.price) as sum_price')
            ->from('drug dr, stock st')
            ->andWhere("st.drug_id = dr.id")
            ->andWhere("dr.drug_type = $drugType")
            ->andWhere('st.deleted_at IS NULL')
            ->groupBy('dr.id')
            ->all();


        /* return (new Query())
            ->select('st.type,dr.name,SUM(st.quantity) as quantity')
            ->from('drug dr, stock st')
            ->andWhere("st.drug_id = dr.id")
            ->andWhere("dr.drug_type = $drugType")
            ->andWhere('st.deleted_at IS NULL')
            ->groupBy('dr.id')
            ->all(); */
    }

    /**
     * @param $type
     * @return array|bool
     * @author Saiat Kalbiev <kalbievich11@gmail.com>
     */
    public static function getDrugsColumnedArrayByDrugType($type)
    {
        $drugCategories = DrugCategory::find()->all();


        $drugs = Drug::find()
            ->select('dr.*')
            ->from('drug dr,drug_category drc')
            ->byDrugType($type)
            ->andWhere('dr.category_id = drc.id')
            ->with('category')
            ->all();

        if (count($drugs) < 0) {
            return false;
        }
        $columns[1] = [];
        $columns[2] = [];
        $columns[3] = [];

        foreach ($drugs as $drug) {
            if ($drug->category->protocol_col_index === 1) {
                if (isset($columns[1][$drug->category->name])) {
                    array_push($columns[1][$drug->category->name], $drug);
                } else {
                    $columns[1][$drug->category->name] = [];
                    array_push($columns[1][$drug->category->name], $drug);
                }
            } else if ($drug->category->protocol_col_index === 2) {
                if (isset($columns[2][$drug->category->name])) {
                    array_push($columns[2][$drug->category->name], $drug);
                } else {
                    $columns[2][$drug->category->name] = [];
                    array_push($columns[2][$drug->category->name], $drug);
                }
            } else if ($drug->category->protocol_col_index === 3) {
                if (isset($columns[3][$drug->category->name])) {
                    array_push($columns[3][$drug->category->name], $drug);
                } else {
                    $columns[3][$drug->category->name] = [];
                    array_push($columns[3][$drug->category->name], $drug);
                }
            }
        }
        return $columns;
    }


    /**
     * @param $protocol Protocol
     * @param $request []
     * @return array
     * @author Saiat Kalbiev <kalbievich11@gmail.com>
     */
    public static function saveProtocolWithExpense($protocol, $request)
    {
        $patientId = $request['Protocol']['patient_id'];
        $sessionDate = strtotime($request['Protocol']['session_date']);
        $transaction = \Yii::$app->db->beginTransaction();
        $protocol->session_date = $sessionDate;
        $protocol->doctor_id = $request['Protocol']['doctor_id'];

        if (!$protocol->load(Yii::$app->request->post()) || !$protocol->save()) {
            $transaction->rollBack();
            return [
                'success' => false,
                'message' => 'ვერ მოხერხდა პროტოკოლის შენახვა',
                'messageType' => 'danger',
            ];
        }

        //save expense
        if (isset($request['expense_drugs'])) {
            foreach ($request['expense_drugs'] as $drugId => $quantity) {
                $quantity = intval($quantity);
                if ($quantity <= 0) {
                    $transaction->rollBack();
                    return [
                        'success' => false,
                        'message' => 'რაოდენობა უნდა იყოს ერთი ან მეტი'
                    ];
                }
                $sum = Stock::find()->byDrugId($drugId)->notDeleted()->sum('quantity');
                if ($sum < $quantity) {
                    $transaction->rollBack();
                    return [
                        'success' => false,
                        'message' => 'პრეპარატის რაოდენობა არ არის ხელმისაწვდომი'
                    ];
                }

                $stocks = Stock::find()->byDrugId($drugId)->notDeleted()->orderBy('created_at ASC')->all();

                $nextQuantity = null;
                foreach ($stocks as $stock) {
                    $expense = new Expense();
                    $expense->stock_id = $stock->id;
                    $expense->patient_id = $patientId;
                    $expense->session_date = $sessionDate;
                    $expense->protocol_id = $protocol->id;
                    $expense->created_at = time();

                    $stock->is_used = 1;
                    $stock->updated_at = time();


                    if ($nextQuantity > 0) {
                        $quantity = $nextQuantity;
                    }
                    if ($stock->quantity === 0) {
                        continue;
                    }

                    if ($stock->quantity >= $quantity) {
                        $stock->quantity = $stock->quantity - $quantity;
                        $expense->quantity = $quantity;
                        if (!$stock->save() || !$expense->save()) {
                            $transaction->rollBack();
                            return [
                                'success' => false,
                                'message' => 'ვერ მოხერხდა მარაგის ან დანახარჯის შენახვა'
                            ];
                        }
                        break;
                    } else {
                        $expense->quantity = $stock->quantity;
                        $nextQuantity = $quantity - $stock->quantity;
                        $stock->quantity = 0;
                        if (!$stock->save() || !$expense->save()) {
                            $transaction->rollBack();
                            return [
                                'success' => false,
                                'message' => 'ვერ მოხერხდა მარაგის ან დანახარჯის შენახვა'
                            ];
                        }
                    }
                }
            }
        }

        $transaction->commit();
        return ['success' => true];
    }

    /**
     * @param $startDate
     * @param $endDate
     * @return array|bool
     * @author Saiat Kalbiev <kalbievich11@gmail.com>
     */
    public static function getExpensesBySessionDatePivoted($startDate, $endDate, $patientId, $drugType)
    {

        $expenses = Expense::find()
            ->select('ex.*')
            ->from('expense ex, stock st , drug dr, patient pt')
            ->andWhere('ex.stock_id = st.id')
            ->andWhere('st.drug_id = dr.id')
            ->andWhere("dr.drug_type = $drugType")
            ->andWhere('ex.patient_id = pt.id');

        if ($patientId) {
            $expenses = $expenses->andWhere("ex.patient_id = $patientId");
        }

        $expenses = $expenses
            ->andWhere("ex.session_date > $startDate")
            ->andWhere("ex.session_date < $endDate")
            ->with(["stock", "stock.drug", "patient"])
            ->all();
        if (count($expenses) < 1) {
            return false;
        }

        $expenseData = [];
        $drugs = [];
        foreach ($expenses as $expense) {
            if (!isset($drugs[$expense->stock->drug->name])) {
                $drugs[$expense->stock->drug->name] = $expense->stock->drug->name;
            }
            if (!isset($expenseData[$expense->patient_id])) {
                $expenseData[$expense->patient_id] = [];
                $expenseData[$expense->patient_id]['name'] = $expense->patient->getFullName();
                $expenseData[$expense->patient_id]['drugs'] = [];
            }
            if (isset($expenseData[$expense->patient_id]['drugs'][$expense->stock->drug->name])) {
                $expenseData[$expense->patient_id]['drugs'][$expense->stock->drug->name] += $expense->quantity;
            } else {
                $expenseData[$expense->patient_id]['drugs'][$expense->stock->drug->name] = $expense->quantity;
            }
        }
        ksort($drugs);
        foreach ($expenseData as $key => $value) {
            $diffArray = array_diff_key($drugs, $expenseData[$key]['drugs']);
            if (count($diffArray) > 0) {
                $diffArray = array_map(function () {
                    return 0;
                }, $diffArray);
                $expenseData[$key]['drugs'] = array_merge($expenseData[$key]['drugs'], $diffArray);
            }
            ksort($expenseData[$key]['drugs']);
        }


        $id = 0;
        foreach ($expenseData as $key => $value) {
            $expenseData[$key]['id'] = $id++;
        }

        return [
            'drugs' => $drugs,
            'expenseData' => $expenseData
        ];
    }

    /**
     * @param $startDate
     * @param $endDate
     * @param $group
     * @return array|null
     * @author Saiat Kalbiev <kalbievich11@gmail.com>
     */
    public static function getReportForMohPrepsAndLabs($startDate, $endDate, $group)
    {

        $expenses = Expense::find()
            ->select('ex.*')
            ->from('expense ex, stock st , drug dr, patient pt')
            ->andWhere('ex.stock_id = st.id')
            ->andWhere('st.drug_id = dr.id')
            ->andWhere('ex.patient_id = pt.id')
            ->andWhere("pt.group = '$group'")
            ->andWhere("dr.drug_type = " . Drug::TYPE_STANDARD)
            ->andWhere("ex.session_date > $startDate")
            ->andWhere("ex.session_date < $endDate")
            ->with(["stock", "stock.drug", "patient"])
            ->all();

        $labs = Lab::find()
            ->select('l.*')
            ->from('lab l, patient pt')
            ->andWhere('l.patient_id = pt.id')
            ->andWhere("pt.group = '$group'")
            ->andWhere("session_date > $startDate")
            ->andWhere("session_date < $endDate")
            ->with(['patient'])
            ->all();

        if (!$expenses && !$labs) {
            return null;
        }

        $arrPrep = [];
        if (count($expenses) > 0) {
            foreach ($expenses as $expense) {
                if (key_exists($expense->patient_id, $arrPrep)) {
                    $arrPrep[$expense->patient_id]['prep'] += $expense->quantity * $expense->stock->price;
                } else {
                    $arrPrep[$expense->patient_id] = [
                        'name' => $expense->patient->getFullName(),
                        'prep' => $expense->quantity * $expense->stock->price,
                        'lab' => 0
                    ];
                }
            }
        }

        foreach ($labs as $lab) {
            if (key_exists($lab->patient_id, $arrPrep)) {
                $arrPrep[$lab->patient_id]['lab'] += $lab->quantity * $lab->price;
            } else {
                $arrPrep[$lab->patient_id] = [
                    'name' => $lab->patient->getFullName(),
                    'prep' => 0,
                    'lab' => $lab->quantity * $lab->price
                ];
            }
        }

        return $arrPrep;
    }

    /**
     * @param $startDate
     * @param $endDate
     * @param $patientId
     * @param $patientGroups
     * @return array|BloodLab[]
     * @author Saiat Kalbiev <kalbievich11@gmail.com>
     */
    public static function getBloodLab($startDate, $endDate, $patientId, $patientGroups)
    {

        $bloodLabs = BloodLab::find()
            ->select('bl.*')
            ->from('blood_lab bl,patient pt')
            ->andWhere('bl.patient_id = pt.id');
        if ($patientId) {
            $bloodLabs = $bloodLabs->andWhere([
                'bl.patient_id' => $patientId
            ]);
        }
        if ($patientGroups) {
            $stmt = "";
            $counter = 0;
            foreach ($patientGroups as $group) {
                if ($counter == 0) {
                    $stmt .= " pt.group = '$group' ";
                } else {
                    $stmt .= " OR pt.group = '$group' ";
                }
                $counter++;
            }
            $bloodLabs = $bloodLabs->andWhere($stmt);
        }
        return $bloodLabs
            ->andWhere("bl.session_date > $startDate AND bl.session_date < $endDate")
            ->with(['patient'])->all();
    }

    /**
     * @param $startDate
     * @param $endDate
     * @param $patientId
     * @param $patientGroups
     * @return array|Lab[]
     * @author Saiat Kalbiev <kalbievich11@gmail.com>
     */
    public static function getLab($startDate, $endDate, $patientId, $patientGroups)
    {

        $labs = Lab::find()
            ->select('l.*')
            ->from('lab l,patient pt')
            ->andWhere('l.patient_id = pt.id');
        if ($patientId) {
            $labs = $labs->andWhere([
                'l.patient_id' => $patientId
            ]);
        }
        if ($patientGroups) {
            $stmt = "";
            $counter = 0;
            foreach ($patientGroups as $group) {
                if ($counter == 0) {
                    $stmt .= " pt.group = '$group' ";
                } else {
                    $stmt .= " OR pt.group = '$group' ";
                }
                $counter++;
            }
            $labs = $labs->andWhere($stmt);
        }
        return $labs
            ->andWhere("l.session_date > $startDate AND l.session_date < $endDate")
            ->with(['patient'])->all();
    }


    public static function getExpensesBySessionDateAndTimePivoted($date, $startTime, $endTime, $patientId, $drugType)
    {

        $expenses = Expense::find()
            ->select('ex.*')
            ->from('expense ex, stock st , drug dr, patient pt,protocol pr')
            ->andWhere('ex.stock_id = st.id')
            ->andWhere('st.drug_id = dr.id')
            ->andWhere("dr.drug_type = $drugType")
            ->andWhere('ex.patient_id = pt.id')
            ->andWhere('pr.id = ex.protocol_id');

        if ($patientId) {
            $expenses = $expenses->andWhere("ex.patient_id = $patientId");
        }
        if ($startTime && $endTime) {
            $expenses = $expenses
                ->andWhere("pr.session_start_time > '$startTime' AND pr.session_end_time < '$endTime' ");
        } else if ($startTime) {
            $expenses = $expenses->andWhere("pr.session_start_time > '$startTime' ");
        } else if ($endTime) {
            $expenses = $expenses->andWhere("pr.session_end_time < '$endTime' ");
        }

        $expenses = $expenses
            ->andWhere("ex.session_date = $date")
            ->with(["stock", "stock.drug", "patient"])
            ->all();
        if (count($expenses) < 1) {
            return false;
        }
        ini_set('display_errors', 1);
        ini_set('display_startup_errors', 1);
        error_reporting(E_ALL);
        $expenseData = [];
        $drugs = [];
        foreach ($expenses as $expense) {
            if (!isset($drugs[$expense->stock->drug->name])) {
                $drugs[$expense->stock->drug->name] = $expense->stock->drug->name;
            }
            if (!isset($expenseData[$expense->patient_id])) {
                $expenseData[$expense->patient_id] = [];
                $expenseData[$expense->patient_id]['name'] = $expense->patient->getFullName();
                $expenseData[$expense->patient_id]['drugs'] = [];
            }
            if (isset($expenseData[$expense->patient_id]['drugs'][$expense->stock->drug->name])) {
                $expenseData[$expense->patient_id]['drugs'][$expense->stock->drug->name] += $expense->quantity;
            } else {
                $expenseData[$expense->patient_id]['drugs'][$expense->stock->drug->name] = $expense->quantity;
            }
        }
        ksort($drugs);
        foreach ($expenseData as $key => $value) {
            $diffArray = array_diff_key($drugs, $expenseData[$key]['drugs']);
            if (count($diffArray) > 0) {
                $diffArray = array_map(function () {
                    return 0;
                }, $diffArray);
                $expenseData[$key]['drugs'] = array_merge($expenseData[$key]['drugs'], $diffArray);
            }
            ksort($expenseData[$key]['drugs']);
        }


        $id = 0;
        foreach ($expenseData as $key => $value) {
            $expenseData[$key]['id'] = $id++;
        }

        return [
            'drugs' => $drugs,
            'expenseData' => $expenseData
        ];
    }


}