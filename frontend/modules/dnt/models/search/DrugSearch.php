<?php

namespace frontend\modules\dnt\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\modules\dnt\models\Drug;

/**
 * DrugSearch represents the model behind the search form about `frontend\modules\dnt\models\Drug`.
 */
class DrugSearch extends Drug
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'drug_type', 'category_id'], 'integer'],
            [['name'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Drug::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if(isset($params['drug_type'])){
            $this->drug_type = $params['drug_type'];
        }


        if (!$this->validate()) {
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'drug_type' => $this->drug_type,
            'category_id' => $this->category_id,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name]);

        return $dataProvider;
    }
}
