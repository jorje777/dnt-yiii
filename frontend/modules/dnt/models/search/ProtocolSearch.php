<?php

namespace frontend\modules\dnt\models\search;

use centigen\base\helpers\QueryHelper;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\modules\dnt\models\Protocol;

/**
 * ProtocolSearch represents the model behind the search form about `frontend\modules\dnt\models\Protocol`.
 */
class ProtocolSearch extends Protocol
{
    public $start_date;
    public $end_date;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'patient_id', 'initial_weight', 'end_weight', 'created_at', 'updated_at', 'deleted_at','start_date','end_date'], 'integer'],
            [['initial_pressure', 'end_pressure', 'ultrafiltrates'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params,$perPatient,$patientId = null)
    {
        $type = $params['type'];

        if($perPatient){
            $query = Protocol::find()->notDeleted()->byType($type)->byPatientId($patientId);

        }else{
            $query = Protocol::find()
                ->select('t1.*')
                ->from(['patient p','protocol t1'])
                ->andWhere('p.id = t1.patient_id')
                ->andWhere('t1.deleted_at is NULL')
                ->byType($type);

            if(isset($params['patient_id']) && $params['patient_id'] !== ""){
                $query->andWhere("t1.patient_id = " . intval($params['patient_id']));
            }
            if(isset($params['patient_type']) && $params['patient_type'] !== ""){
                $patientType = $params['patient_type'];
                $query->andWhere("p.group = '$patientType'");
            }

            $andWhereStartDate = '';
            $andWhereEndDate = '';
            if(isset($params['start_date']) && $params['end_date']) {
                $time = strtotime($params['start_date']);
                $andWhereStartDate = " AND (`session_date` >= {$time}) ";
            }
            if(isset($params['end_date']) && $params['end_date']) {
                $time = strtotime($params['end_date']);
                $andWhereEndDate = " AND (`session_date` <= {$time}) ";
            }

            $query->join("inner join", "
                (SELECT patient_id, MAX(created_at) created_at
                FROM protocol
                WHERE deleted_at is null
                AND type = $type " . $andWhereStartDate . $andWhereEndDate . "
                GROUP BY patient_id ) t2",
                "t1.patient_id = t2.patient_id AND t1.created_at = t2.created_at");

        }

        if(isset($params['start_date']) && $params['end_date']) {
            $query->andWhere(['>=','session_date',strtotime($params['start_date'])]);
        }
        if(isset($params['end_date']) && $params['end_date']) {
            $query->andWhere(['<=','session_date',strtotime($params['end_date'])]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);


        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'patient_id' => $this->patient_id,
            'session_date' => $this->session_date,
            'initial_weight' => $this->initial_weight,
            'end_weight' => $this->end_weight,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'deleted_at' => $this->deleted_at,
        ]);

        $query->andFilterWhere(['like', 'initial_pressure', $this->initial_pressure])
            ->andFilterWhere(['like', 'end_pressure', $this->end_pressure])
            ->andFilterWhere(['like', 'ultrafiltrates', $this->ultrafiltrates]);

        return $dataProvider;
    }
}
