<?php

namespace frontend\modules\dnt\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\modules\dnt\models\Stock;

/**
 * StockSearch represents the model behind the search form about `frontend\modules\dnt\models\Stock`.
 */
class StockSearch extends Stock
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'drug_id', 'quantity', 'type', 'created_at', 'updated_at', 'deleted_at'], 'integer'],
            [['price'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params,$drug_type)
    {
        $query = Stock::find()
            ->select(['stock.id as id','stock.drug_id as drug_id','SUM(quantity) as quantity'])
            ->from('stock,drug')
            ->andWhere('stock.drug_id = drug.id')
            ->andWhere(['drug_type' => $drug_type])
            ->notDeleted()
            ->groupBy('drug_id');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'drug_id' => $this->drug_id,
            'quantity' => $this->quantity,
            'price' => $this->price,
            'type' => $this->type,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'deleted_at' => $this->deleted_at,
        ]);

        return $dataProvider;
    }
}
