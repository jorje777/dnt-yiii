<?php

namespace frontend\modules\dnt\models;

use Yii;

/**
 * This is the model class for table "patient".
 *
 * @property integer $id
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $deleted_at
 * @property string $name
 * @property string $surname
 * @property string $personal_id
 * @property string $birth_date
 * @property string $group
 * @property string $transplant_date
 * @property string $mors
 * @property string $group_rezus
 * @property Lab $lab[]
 */

class Patient extends \yii\db\ActiveRecord
{
    const GROUPS = [
        'პ.დ' => 'პ.დ',
        'ჰ.დ' => 'ჰ.დ',
        'მწვავე' => 'მწვავე'
    ];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'patient';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'surname', 'personal_id', 'birth_date', 'group'], 'required'],
            [['birth_date', 'transplant_date', 'mors'], 'safe'],
            [['name', 'surname'], 'string', 'max' => 127],
            [['personal_id', 'group_rezus'], 'string', 'max' => 31],
            [['group'], 'string', 'max' => 15],
            [['personal_id'],'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('dnt', 'ID'),
            'name' => Yii::t('dnt', 'სახელი'),
            'surname' => Yii::t('dnt', 'გვარი'),
            'personal_id' => Yii::t('dnt', 'პირადობის ნომერი'),
            'birth_date' => Yii::t('dnt', 'დაბადების თარიღი'),
            'group' => Yii::t('dnt', 'ჯგუფი და რეზუსი'),
            'transplant_date' => Yii::t('dnt', 'გადანერგვის თარიღი'),
            'mors' => Yii::t('dnt', 'Mors'),
            'group_rezus' => Yii::t('dnt', 'ჯგუფი და რეზუსი'),
        ];
    }

    /**
     * @inheritdoc
     * @return \frontend\modules\dnt\models\query\PatientQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \frontend\modules\dnt\models\query\PatientQuery(get_called_class());
    }

    /**
     * @return bool
     * @author Saiat Kalbiev <kalbievich11@gmail.com>
     */
    public function markDeleted(){
        $this->deleted_at = time();
        if($this->save()){
            return true;
        }
        return false;
    }
    /**
     * @param bool $insert
     * @return bool
     * @author Saiat Kalbiev <kalbievich11@gmail.com>
     */
    public function beforeSave($insert)
    {
        $controllerId = Yii::$app->controller->action->id;
        if($controllerId == "create"){
            $this->created_at = time();
        }
        else if($controllerId == "update"){
            $this->updated_at = time();
        }
        return parent::beforeSave($insert);
    }

    /**
     * @return string
     * @author Saiat Kalbiev <kalbievich11@gmail.com>
     */
    public function getFullName(){
        return $this->surname ." ". $this->name;
    }
}
