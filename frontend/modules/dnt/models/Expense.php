<?php

namespace frontend\modules\dnt\models;

use Yii;

/**
 * This is the model class for table "expense".
 *
 * @property integer $id
 * @property integer $stock_id
 * @property integer $patient_id
 * @property integer $quantity
 * @property string $comment
 * @property integer $session_date
 * @property integer $created_at
 * @property integer $protocol_id
 *
 * @property Patient $patient
 * @property Stock $stock
 */
class Expense extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'expense';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['stock_id', 'patient_id', 'quantity', 'session_date', 'created_at'], 'required'],
            [['stock_id', 'patient_id', 'quantity', 'session_date', 'created_at','protocol_id'], 'integer'],
            [['comment'], 'string'],
            [['patient_id'], 'exist', 'skipOnError' => true, 'targetClass' => Patient::className(), 'targetAttribute' => ['patient_id' => 'id']],
            [['stock_id'], 'exist', 'skipOnError' => true, 'targetClass' => Stock::className(), 'targetAttribute' => ['stock_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('dnt', 'ID'),
            'stock_id' => Yii::t('dnt', 'Stock ID'),
            'patient_id' => Yii::t('dnt', 'Patient ID'),
            'quantity' => Yii::t('dnt', 'Quantity'),
            'comment' => Yii::t('dnt', 'Comment'),
            'session_date' => Yii::t('dnt', 'Session Date'),
            'created_at' => Yii::t('dnt', 'Created At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPatient()
    {
        return $this->hasOne(Patient::className(), ['id' => 'patient_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProtocol()
    {
        return $this->hasOne(Protocol::className(), ['id' => 'protocol_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStock()
    {
        return $this->hasOne(Stock::className(), ['id' => 'stock_id']);
    }

    /**
     * @inheritdoc
     * @return \frontend\modules\dnt\models\query\ExpenseQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \frontend\modules\dnt\models\query\ExpenseQuery(get_called_class());
    }
}
