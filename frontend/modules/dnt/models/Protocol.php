<?php

namespace frontend\modules\dnt\models;

use Yii;

/**
 * This is the model class for table "protocol".
 *
 * @property integer $id
 * @property integer $doctor_id
 * @property integer $patient_id
 * @property integer $session_date
 * @property integer $initial_weight
 * @property integer $end_weight
 * @property string $initial_pressure
 * @property string $end_pressure
 * @property string $ultrafiltrates
 * @property string $session_start_time
 * @property string $session_end_time
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $deleted_at
 * @property integer $type
 *
 * @property Patient $patient
 * @property Doctor $doctor
 */
class Protocol extends \yii\db\ActiveRecord
{
    const TYPE_STATE = 1;
    const TYPE_MWVAVE = 2;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'protocol';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['patient_id','type'], 'required'],
            [['initial_weight', 'end_weight'],'safe'],
            [['patient_id', 'created_at', 'updated_at', 'deleted_at','doctor_id'], 'integer'],
            [['initial_pressure', 'end_pressure'], 'string', 'max' => 10],
            [['ultrafiltrates','session_start_time','session_end_time'], 'string', 'max' => 15],
            [['patient_id'], 'exist', 'skipOnError' => true, 'targetClass' => Patient::className(), 'targetAttribute' => ['patient_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('dnt', 'ID'),
            'patient_id' => Yii::t('dnt', 'სახელი,გვარი'),
            'doctor_id' => Yii::t('dnt', 'ექთანი'),
            'session_date' => Yii::t('dnt', 'სეანსის თარიღი'),
            'initial_weight' => Yii::t('dnt', 'წონა '),
            'end_weight' => Yii::t('dnt', 'წონა '),
            'initial_pressure' => Yii::t('dnt', 'არტერიული წნევა '),
            'end_pressure' => Yii::t('dnt', 'არტერიული წნევა '),
            'session_start_time' => Yii::t('dnt', 'დასაწყისი დრო'),
            'session_end_time' => Yii::t('dnt', 'დასასრული დრო'),
            'ultrafiltrates' => Yii::t('dnt', 'ულტრაფილტრატის რაოდენობა'),
            'created_at' => Yii::t('dnt', 'Created At'),
            'updated_at' => Yii::t('dnt', 'Updated At'),
            'deleted_at' => Yii::t('dnt', 'Deleted At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPatient()
    {
        return $this->hasOne(Patient::className(), ['id' => 'patient_id']);
    }

    public function getDoctor()
    {
        return $this->hasOne(Doctor::className(), ['id' => 'doctor_id']);
    }

    /**
     * @inheritdoc
     * @return \frontend\modules\dnt\models\query\ProtocolQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \frontend\modules\dnt\models\query\ProtocolQuery(get_called_class());
    }

    /**
     * @param bool $insert
     * @return bool
     * @author Saiat Kalbiev <kalbievich11@gmail.com>
     */
    public function beforeSave($insert)
    {
        $this->session_date = is_int($this->session_date) ? $this->session_date : strtotime($this->session_date);


        $controllerId = Yii::$app->controller->action->id;
        if($controllerId == "create"){
            $this->created_at = time();
        }
        else if($controllerId == "update"){
            $this->updated_at = time();
        }
        if($this->session_start_time == ""){
            $this->session_start_time = null;
        }
        if($this->session_end_time == ""){
            $this->session_end_time = null;
        }
        return parent::beforeSave($insert);
    }

    /**
     * @return bool
     * @author Saiat Kalbiev <kalbievich11@gmail.com>
     */
    public function markDeleted(){
        $this->deleted_at = time();
        if($this->save()){
            return true;
        }
        return false;
    }

    public static function getProtocolTypes(){
        return [
            self::TYPE_STATE => 'სახელმწიფო',
            self::TYPE_MWVAVE => 'სახელმწიფო მწვავე'
        ];
    }
}
