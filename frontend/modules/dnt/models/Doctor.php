<?php

namespace frontend\modules\dnt\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%doctor}}".
 *
 * @property integer $id
 * @property string $name
 * @property string $surname
 *
 * @property Protocol[] $protocols
 */
class Doctor extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%doctor}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'surname'], 'string', 'max' => 1027],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'surname' => 'Surname',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProtocols()
    {
        return $this->hasMany(Protocol::className(), ['doctor_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return \frontend\modules\dnt\models\query\DoctorQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \frontend\modules\dnt\models\query\DoctorQuery(get_called_class());
    }

    public static function getDoctorsMapped()
    {
        $doctors = self::find()->all();
        $return = [];
        foreach ($doctors as $doctor) {
            $return[$doctor->id] = $doctor->getFullName();
        }
        return $return;
    }

    public function getFullName()
    {
        return $this->surname . " " . $this->name;
    }
}
