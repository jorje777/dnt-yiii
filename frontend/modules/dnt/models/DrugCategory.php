<?php

namespace frontend\modules\dnt\models;

use Yii;

/**
 * This is the model class for table "drug_category".
 *
 * @property integer $id
 * @property string $name
 * @property integer $is_mwvave
 * @property integer $protocol_col_index
 */
class DrugCategory extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'drug_category';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name','protocol_col_index'], 'required'],
            [['name', 'is_mwvave'], 'unique', 'targetAttribute' => ['name', 'is_mwvave']],
            [['is_mwvave','protocol_col_index'], 'integer'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('dnt', 'ID'),
            'name' => Yii::t('dnt', 'Name'),
            'is_mwvave' => Yii::t('dnt', 'Is Mwvave'),
        ];
    }

    /**
     * @inheritdoc
     * @return \frontend\modules\dnt\models\query\DrugCategoryQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \frontend\modules\dnt\models\query\DrugCategoryQuery(get_called_class());
    }
}
