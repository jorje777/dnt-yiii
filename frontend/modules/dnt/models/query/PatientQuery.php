<?php

namespace frontend\modules\dnt\models\query;

use frontend\modules\dnt\models\Patient;

/**
 * This is the ActiveQuery class for [[\frontend\modules\dnt\models\Patient]].
 *
 * @see \frontend\modules\dnt\models\Patient
 */
class PatientQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return \frontend\modules\dnt\models\Patient[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \frontend\modules\dnt\models\Patient|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    /**
     * @param $id
     * @return self
     * @author Saiat Kalbiev <kalbievich11@gmail.com>
     */
    public function byId($id){
        return $this->andWhere(['id' => $id]);
    }

    /**
     * @return self
     * @author Saiat Kalbiev <kalbievich11@gmail.com>
     */
    public function notDeleted(){
        return $this->andWhere(['deleted_at' => null]);
    }
}
