<?php

namespace frontend\modules\dnt\models\query;

/**
 * This is the ActiveQuery class for [[\frontend\modules\dnt\models\Doctor]].
 *
 * @see \frontend\modules\dnt\models\Doctor
 */
class DoctorQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return \frontend\modules\dnt\models\Doctor[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \frontend\modules\dnt\models\Doctor|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
