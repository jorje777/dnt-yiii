<?php

namespace frontend\modules\dnt\models\query;

/**
 * This is the ActiveQuery class for [[\frontend\modules\dnt\models\Protocol]].
 *
 * @see \frontend\modules\dnt\models\Protocol
 */
class ProtocolQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return \frontend\modules\dnt\models\Protocol[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \frontend\modules\dnt\models\Protocol|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    /**
     * @param $id
     * @return self
     * @author Saiat Kalbiev <kalbievich11@gmail.com>
     */
    public function byId($id){
        return $this->andWhere(['id' => $id]);
    }

    /**
     * @return self
     * @author Saiat Kalbiev <kalbievich11@gmail.com>
     */
    public function notDeleted(){
        return $this->andWhere(['deleted_at' => null]);
    }

    /**
     * @param $type
     * @return $this
     * @author Saiat Kalbiev <kalbievich11@gmail.com>
     */
    public function byType($type){
        return $this->andWhere(['type' => $type]);
    }
    /**
     * @param $type
     * @return $this
     * @author Saiat Kalbiev <kalbievich11@gmail.com>
     */
    public function byPatientId($id){
        return $this->andWhere(['patient_id' => $id]);
    }
}
