<?php

namespace frontend\modules\dnt\models\query;

/**
 * This is the ActiveQuery class for [[\frontend\modules\dnt\models\DrugCategory]].
 *
 * @see \frontend\modules\dnt\models\DrugCategory
 */
class DrugCategoryQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return \frontend\modules\dnt\models\DrugCategory[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \frontend\modules\dnt\models\DrugCategory|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    /**
     * @param $condition
     * @return self
     * @author Saiat Kalbiev <kalbievich11@gmail.com>
     */
    public function byMwvave($condition){
        if($condition == true ){
            return $this->andWhere(['is_mwvave' => 1]);
        }
        return $this->andWhere(['is_mwvave' => 0]);
    }
}
