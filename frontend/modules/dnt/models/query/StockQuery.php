<?php

namespace frontend\modules\dnt\models\query;

/**
 * This is the ActiveQuery class for [[\frontend\modules\dnt\models\Stock]].
 *
 * @see \frontend\modules\dnt\models\Stock
 */
class StockQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return \frontend\modules\dnt\models\Stock[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \frontend\modules\dnt\models\Stock|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    /**
     * @param $id
     * @return $this
     * @author Saiat Kalbiev <kalbievich11@gmail.com>
     */
    public function byId($id){
        return $this->andWhere(['id' => $id]);
    }

    /**
     * @param $drugId
     * @return $this
     * @author Saiat Kalbiev <kalbievich11@gmail.com>
     */
    public function byDrugId($drugId){
        return $this->andWhere(['drug_id' => $drugId]);
    }

    /**
     * @return $this
     * @author Saiat Kalbiev <kalbievich11@gmail.com>
     */
    public function notDeleted(){
        return $this->andWhere(['deleted_at' => null]);
    }

    /**
     * @return $this
     * @author Saiat Kalbiev <kalbievich11@gmail.com>
     */
    public function notSoldOut(){
        return $this->andWhere(['>','quantity',0 ]);
    }

}
