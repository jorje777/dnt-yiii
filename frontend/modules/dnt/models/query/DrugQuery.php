<?php

namespace frontend\modules\dnt\models\query;

/**
 * This is the ActiveQuery class for [[\frontend\modules\dnt\models\Drug]].
 *
 * @see \frontend\modules\dnt\models\Drug
 */
class DrugQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return \frontend\modules\dnt\models\Drug[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \frontend\modules\dnt\models\Drug|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    /**
     * @param $id
     * @return $this
     * @author Saiat Kalbiev <kalbievich11@gmail.com>
     */
    public function byId($id){
        return $this->andWhere(['id' => $id]);
    }

    /**
     * @param $drugType
     * @return $this
     */
    public function byDrugType($drugType){
        return $this->andWhere(['drug_type' => $drugType]);
    }
}
