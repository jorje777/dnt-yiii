<?php

namespace frontend\modules\dnt\models;

use Yii;

/**
 * This is the model class for table "stock".
 *
 * @property integer $id
 * @property integer $drug_id
 * @property integer $quantity
 * @property string $price
 * @property integer $type
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $deleted_at
 * @property integer $is_used
 * @property integer $initial_quantity
 *
 * @property Drug $drug
 */
class Stock extends \yii\db\ActiveRecord
{
    const TYPE_PIECES = 1;
    const TYPE_LITRES = 2;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'stock';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['drug_id', 'quantity', 'price', 'type'], 'required'],
            [['drug_id', 'quantity', 'type', 'created_at', 'updated_at', 'deleted_at','is_used','initial_quantity'], 'integer'],
            [['price'], 'number'],
            [['drug_id'], 'exist', 'skipOnError' => true, 'targetClass' => Drug::className(), 'targetAttribute' => ['drug_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('dnt', 'ID'),
            'drug_id' => Yii::t('dnt', 'პრეპარატის სახელი'),
            'quantity' => Yii::t('dnt', 'რაოდენობა'),
            'price' => Yii::t('dnt', 'ფასი'),
            'type' => Yii::t('dnt', 'ტიპი'),
            'created_at' => Yii::t('dnt', 'Created At'),
            'updated_at' => Yii::t('dnt', 'Updated At'),
            'deleted_at' => Yii::t('dnt', 'Deleted At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDrug()
    {
        return $this->hasOne(Drug::className(), ['id' => 'drug_id']);
    }

    /**
     * @inheritdoc
     * @return \frontend\modules\dnt\models\query\StockQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \frontend\modules\dnt\models\query\StockQuery(get_called_class());
    }


    /**
     * @param bool $insert
     * @return bool
     * @author Saiat Kalbiev <kalbievich11@gmail.com>
     */
    public function beforeSave($insert)
    {
        $actionId = Yii::$app->controller->action->id;
        $controllerId = Yii::$app->controller->id;
        if($actionId == "create" && !($controllerId == 'protocol')){
            $this->created_at = time();
            $this->initial_quantity = $this->quantity;
        }else if($actionId = 'update'){
            $this->updated_at = time();
            if($this->is_used == 0){
                $this->initial_quantity = $this->quantity;
            }
        }
        return parent::beforeSave($insert);
    }

    /**
     * @return array
     * @author Saiat Kalbiev <kalbievich11@gmail.com>
     */
    public static function getQuantityTypeConstants(){
        return [
            1 => 'ცალი',
            2 => 'მლ ლიტრი',
            3 => 'გრამი'
        ];
    }

    /**
     * @return bool
     * @author Saiat Kalbiev <kalbievich11@gmail.com>
     */
    public function markDeleted(){
        $this->deleted_at = time();
        if($this->save()){
            return true;
        }
        return false;
    }

}
