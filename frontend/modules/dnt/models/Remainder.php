<?php

namespace frontend\modules\dnt\models;

use Yii;

/**
 * This is the model class for table "remainder".
 *
 * @property integer $id
 * @property string $drug_name
 * @property integer $type
 * @property integer $quantity
 * @property integer $date
 * @property integer $stock_type
 * @property integer $sum_price
 */
class Remainder extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'remainder';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['drug_name', 'type', 'quantity', 'date'], 'required'],
            [['type', 'date','stock_type'], 'integer'],
            [['drug_name'], 'string', 'max' => 255],
            [['quantity','sum_price'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('dnt', 'ID'),
            'drug_name' => Yii::t('dnt', 'Drug Name'),
            'type' => Yii::t('dnt', 'Type'),
            'quantity' => Yii::t('dnt', 'Quantity'),
            'date' => Yii::t('dnt', 'Date'),
            'stock_type' => Yii::t('dnt', 'ტიპი'),
            'sum_price' => Yii::t('dnt', 'Sum price'),
        ];
    }

    /**
     * @inheritdoc
     * @return \frontend\modules\dnt\models\query\RemainderQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \frontend\modules\dnt\models\query\RemainderQuery(get_called_class());
    }
}
