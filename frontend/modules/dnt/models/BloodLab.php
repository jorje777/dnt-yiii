<?php

namespace frontend\modules\dnt\models;

use Yii;

/**
 * This is the model class for table "blood_lab".
 *
 * @property integer $id
 * @property string $name
 * @property integer $quantity
 * @property string $price
 * @property integer $session_date
 * @property integer $created_at
 * @property integer $patient_id
 *
 * @property Patient $patient
 */
class BloodLab extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'blood_lab';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'quantity', 'price', 'session_date', 'created_at'], 'required'],
            [['quantity', 'session_date', 'created_at','patient_id'], 'integer'],
            [['price'], 'number'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'quantity' => 'Quantity',
            'price' => 'Price',
            'session_date' => 'Session Date',
            'created_at' => 'Created At',
        ];
    }

    /**
     * @inheritdoc
     * @return \frontend\modules\dnt\models\query\BloodLabQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \frontend\modules\dnt\models\query\BloodLabQuery(get_called_class());
    }

    /**
     * @param $params
     * @param $sessionDate
     * @return bool
     */
    public function customLabSave($params, $sessionDate,$patientId){
        $this->patient_id = $patientId;
        $this->name = $params['name'];
        $this->quantity = $params['quantity'];
        $this->price = $params['price'];
        $this->session_date = $sessionDate;
        $this->created_at = time();
        if($this->save()){
            return true;
        }
        return false;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPatient()
    {
        return $this->hasOne(Patient::className(), ['id' => 'patient_id']);
    }
}
