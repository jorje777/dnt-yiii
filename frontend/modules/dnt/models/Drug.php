<?php

namespace frontend\modules\dnt\models;

use Yii;

/**
 * This is the model class for table "drug".
 *
 * @property integer $id
 * @property string $name
 * @property integer $drug_type
 * @property integer $category_id
 *
 * @property DrugCategory $category
 */
class Drug extends \yii\db\ActiveRecord
{
    const TYPE_STANDARD= 0;
    const TYPE_STATE = 1;
    const TYPE_STATE_MWVAVE = 2;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'drug';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'drug_type'], 'required'],
            [['drug_type', 'category_id'], 'integer'],
            [['name', 'drug_type'], 'unique', 'targetAttribute' => ['name', 'drug_type']],
            [['name'], 'string', 'max' => 255],
            [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => DrugCategory::className(), 'targetAttribute' => ['category_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('dnt', 'ID'),
            'name' => Yii::t('dnt', 'სახელი'),
            'drug_type' => Yii::t('dnt', 'ტიპი'),
            'category_id' => Yii::t('dnt', 'კატეგორია'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(DrugCategory::className(), ['id' => 'category_id']);
    }

    /**
     * @inheritdoc
     * @return \frontend\modules\dnt\models\query\DrugQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \frontend\modules\dnt\models\query\DrugQuery(get_called_class());
    }

    /**
     * @return array
     * @author Saiat Kalbiev <kalbievich11@gmail.com>
     */
    public static function getDrugTypes(){
        return [
            Drug::TYPE_STANDARD => "სტანდარტული",
            Drug::TYPE_STATE => "სახელმწიფო",
            Drug::TYPE_STATE_MWVAVE => "სახელმწიფო მწვავე"
        ];
    }
}
