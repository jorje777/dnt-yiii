<?php

use yii\db\Migration;

class m180731_080541_alter_table_protocol_add_column_doctor_id extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%protocol}}','doctor_id',$this->integer(11));
        $this->addForeignKey(
            'fk_protocol_doctor',
            '{{%protocol}}',
            'doctor_id',
            '{{%doctor}}',
            'id',
            'NO ACTION'
        );
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk_protocol_doctor','{{%protocol}}');
        $this->dropColumn('{{%protocol}}','doctor_id');
    }
}
