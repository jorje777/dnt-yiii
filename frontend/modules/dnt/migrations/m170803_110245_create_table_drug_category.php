<?php

use yii\db\Migration;

class m170803_110245_create_table_drug_category extends Migration
{
    public function safeUp()
    {
        $this->createTable('drug_category', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->notNull(),
            'is_mwvave' => $this->integer(1)->defaultValue(0)->notNull()
        ]);
    }

    public function safeDown()
    {
        $this->dropTable('drug_category');
    }
}
