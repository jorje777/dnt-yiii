<?php

use yii\db\Migration;

class m180204_091240_alter_table_protocol_change_weight_columns_to_decimal extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('{{%protocol}}','initial_weight',$this->decimal(25,5));
        $this->alterColumn('{{%protocol}}','end_weight',$this->decimal(25,5));
    }

    public function safeDown()
    {

    }

}
