<?php

use yii\db\Migration;

class m170809_180751_create_table_lab extends Migration
{
    public function safeUp()
    {
        $this->createTable('lab',[
            'id' => $this->primaryKey(),
            'patient_id' => $this->integer(11)->notNull(),
            'name' => $this->string(255)->notNull(),
            'quantity' => $this->integer(11)->notNull(),
            'price' => $this->decimal(30,5)->notNull(),
            'session_date' => $this->integer(11)->notNull(),
            'created_at' => $this->integer(11)->notNull()
        ]);
        
        $this->addForeignKey(
            'fk_lab_patient',
            'lab',
            'patient_id',
            'patient',
            'id',
            'NO ACTION'
        );
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk_lab_patient','lab');
        $this->dropTable('lab');
    }
}
