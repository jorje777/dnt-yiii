<?php

use yii\db\Migration;

class m170803_115516_create_table_drug extends Migration
{
    public function safeUp()
    {
        $this->createTable('drug',[
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->notNull(),
            'drug_type' => $this->integer(1)->notNull(),
            'category_id' => $this->integer(11)->null()
        ]);

        $this->addForeignKey(
            'fk_drug_drug_category',
            'drug',
            'category_id',
            'drug_category',
            'id',
            'NO ACTION'
        );
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk_drug_drug_category','drug');
        $this->dropTable('drug');
    }
}
