<?php

use yii\db\Migration;

class m180802_143648_alter_table_remainder_make_quantity_decimal extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('{{%remainder}}','quantity',$this->decimal(30,5)->notNull());
    }

    public function safeDown()
    {
        $this->alterColumn('{{%remainder}}','quantity',$this->integer(11)->notNull());
    }
}
