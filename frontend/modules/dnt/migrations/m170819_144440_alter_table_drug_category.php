<?php

use yii\db\Migration;

class m170819_144440_alter_table_drug_category extends Migration
{
    public function safeUp()
    {
        $this->addColumn('drug_category','protocol_col_index',$this->integer(1)->notNull());
    }

    public function safeDown()
    {
        $this->dropColumn('drug_category','protocol_col_index');
    }

}
