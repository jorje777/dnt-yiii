<?php

use yii\db\Migration;

class m170805_192008_create_table_stock extends Migration
{
    public function safeUp()
    {
        $this->createTable("stock",[
            'id' => $this->primaryKey(),
            'drug_id' => $this->integer(11)->notNull(),
            'quantity' => $this->integer(11)->notNull(),
            'price' => $this->decimal(31,11)->notNull(),
            'type' => $this->integer(1)->notNull(),
            'created_at' => $this->integer(11)->notNull(),
            'updated_at' => $this->integer(11)->null(),
            'deleted_at' => $this->integer(11)->null()
        ]);

        $this->addForeignKey(
          'fk_stock_drug_id',
          'stock',
          'drug_id',
          'drug',
          'id',
          'NO ACTION'
        );

    }

    public function safeDown()
    {
        $this->dropForeignKey('fk_stock_drug_id','stock');
        $this->dropTable('stock');
    }
}
