<?php

use yii\db\Migration;

class m170809_184526_create_table_blood_lab extends Migration
{
    public function safeUp()
    {
        $this->createTable('blood_lab', [
            'id' => $this->primaryKey(),
            'patient_id' => $this->integer(11)->notNull(),
            'name' => $this->string(255)->notNull(),
            'quantity' => $this->integer(11)->notNull(),
            'price' => $this->decimal(30, 5)->notNull(),
            'session_date' => $this->integer(11)->notNull(),
            'created_at' => $this->integer(11)->notNull()
        ]);

        $this->addForeignKey(
            'fk_blood_lab_patient',
            'blood_lab',
            'patient_id',
            'patient',
            'id',
            'NO ACTION'
        );
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk_blood_lab_patient','blood_lab');
        $this->dropTable('blood_lab');
    }
}
