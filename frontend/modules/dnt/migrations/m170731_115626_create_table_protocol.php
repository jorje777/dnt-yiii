<?php

use yii\db\Migration;

class m170731_115626_create_table_protocol extends Migration
{
    public function safeUp()
    {
        $this->createTable('protocol',[
            'id' => $this->primaryKey(),
            'patient_id' => $this->integer(11)->notNull(),
            'session_date' => $this->integer(11)->notNull(),
            'initial_weight' => $this->integer(3),
            'end_weight' => $this->integer(3),
            'initial_pressure' => $this->string(10),
            'end_pressure' => $this->string(10),
            'ultrafiltrates' => $this->string(15),
            'created_at' => $this->integer(11)->notNull(),
            'updated_at' => $this->integer(11),
            'deleted_at' => $this->integer(11)
        ]);

        $this->addForeignKey(
            'protocol_patient_fk',
            'protocol',
            'patient_id',
            'patient',
            'id',
            'NO ACTION');
    }

    public function safeDown()
    {
        $this->dropForeignKey('protocol_patient_fk','protocol');
        $this->dropTable('protocol');
    }
}
