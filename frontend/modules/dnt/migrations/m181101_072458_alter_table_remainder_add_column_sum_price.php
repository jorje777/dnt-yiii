<?php

use yii\db\Migration;

class m181101_072458_alter_table_remainder_add_column_sum_price extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%remainder}}','sum_price',$this->decimal(30, 7)->defaultValue(1)->after('quantity'));
    }

    public function safeDown()
    {
        $this->dropColumn('{{%remainder}}','sum_price');
    }
}
