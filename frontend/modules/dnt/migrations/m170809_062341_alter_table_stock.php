<?php

use yii\db\Migration;

class m170809_062341_alter_table_stock extends Migration
{
    public function safeUp()
    {
        $this->addColumn('stock','is_used',$this->integer(1)->notNull()->defaultValue(0));
        $this->addColumn('stock','initial_quantity',$this->integer(11)->notNull());
    }

    public function safeDown()
    {
        $this->dropColumn('stock','is_used');
        $this->dropColumn('stock','initial_quantity');
    }
}
