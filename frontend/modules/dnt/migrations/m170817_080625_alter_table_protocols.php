<?php

use yii\db\Migration;

class m170817_080625_alter_table_protocols extends Migration
{
    public function safeUp()
    {
        $this->addColumn('protocol','session_start_time',$this->string(15)->null());
        $this->addColumn('protocol','session_end_time',$this->string(15)->null());
        $this->addColumn('protocol','type',$this->integer(1)->notNull());
    }

    public function safeDown()
    {
        $this->dropColumn('protocol','session_start_time');
        $this->dropColumn('protocol','session_end_time');
        $this->dropColumn('protcol','type');
    }

}
