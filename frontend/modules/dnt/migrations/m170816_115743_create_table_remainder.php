<?php

use yii\db\Migration;

class m170816_115743_create_table_remainder extends Migration
{
    public function safeUp()
    {
        $this->createTable('remainder', [
            'id' => $this->primaryKey(),
            'drug_name' => $this->string(255)->notNull(),
            'type' => $this->integer(1)->notNull(),
            'quantity' => $this->integer(11)->notNull(),
            'date' => $this->integer(11)->notNull()
        ]);
    }

    public function safeDown()
    {
        $this->dropTable('remainder');
    }

}
