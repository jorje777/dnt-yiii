<?php

use yii\db\Migration;

class m170809_070944_create_table_expense extends Migration
{
    public function safeUp()
    {
        $this->createTable('expense', [
            'id' => $this->primaryKey(),
            'stock_id' => $this->integer(11)->notNull(),
            'patient_id' => $this->integer(11)->notNull(),
            'quantity' => $this->integer(11)->notNull(),
            'comment' => $this->text()->null(),
            'session_date' => $this->integer()->notNull(),
            'created_at' => $this->integer(11)->notNull()
        ]);

        $this->addForeignKey(
            'fk_expense_stock',
            'expense',
            'stock_id',
            'stock',
            'id',
            'NO ACTION'
        );

        $this->addForeignKey(
            'fk_expense_patient',
            'expense',
            'patient_id',
            'patient',
            'id',
            'NO ACTION'
        );
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk_expense_patien','expense');
        $this->dropForeignKey('fk_expense_stock','expense');
        $this->dropTable('expense');
    }
}
