<?php

use yii\db\Migration;

class m170720_104123_alter_table_patients extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%patient}}',[
            'id' => $this->primaryKey(),
            'name' => $this->string(127)->notNull(),
            'surname' => $this->string(127)->notNull(),
            'personal_id' => $this->string(31)->notNull(),
            'birth_date' => $this->date()->notNull(),
            'group' => $this->string(15)->notNull(),
            'transplant_date' => $this->date()->null(),
            'mors' => $this->date()->null(),
            'group_rezus' => $this->string(31)->null(),
            'created_at' => $this->integer(11)->notNull(),
            'updated_at' => $this->integer(11),
            'deleted_at' => $this->integer(11)
        ]);
    }

    public function safeDown()
    {
        $this->dropTable('{{%patient}}');
    }
}
