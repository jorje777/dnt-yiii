<?php

use yii\db\Migration;

class m180731_080249_create_table_doctor extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%doctor}}',[
            'id' => $this->primaryKey(),
            'name' => $this->string(1027),
            'surname' => $this->string(1027),
        ]);
    }

    public function safeDown()
    {
        $this->dropTable('{{%doctor}}');
    }
}
