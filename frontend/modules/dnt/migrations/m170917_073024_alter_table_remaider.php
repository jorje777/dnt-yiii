<?php

use yii\db\Migration;

class m170917_073024_alter_table_remaider extends Migration
{
    public function safeUp()
    {
        $this->addColumn('remainder','stock_type',$this->integer(1)->defaultValue(1));
    }

    public function safeDown()
    {
        $this->dropColumn('remainder','stock_type');
    }

}
