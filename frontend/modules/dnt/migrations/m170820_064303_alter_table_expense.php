<?php

use yii\db\Migration;

class m170820_064303_alter_table_expense extends Migration
{
    public function safeUp()
    {
        $this->addColumn('expense','protocol_id',$this->integer(11)->null());
        $this->addForeignKey(
            'fk_expense_protocol',
            'expense',
            'protocol_id',
            'protocol',
            'id',
            'NO ACTION'
        );
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk_expense_protocol','expense');
        $this->dropColumn('expense','protocol_id');
    }
}
