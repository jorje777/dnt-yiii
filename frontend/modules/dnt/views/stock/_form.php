<?php

use kartik\select2\Select2;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\modules\dnt\models\Stock */
/* @var $form yii\widgets\ActiveForm */
/* @var $drugs [] */
/* @var $types [] */
?>

<div class="stock-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'drug_id')->widget(Select2::classname(), [
                'data' => $drugs,
                'options' => ['placeholder' => 'აირჩიე პრეპარატი..'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'quantity')->textInput() ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'price')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'type')->dropDownList($types) ?>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-md-4 col-md-offset-4">
            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? Yii::t('dnt', 'შექმნა ') : Yii::t('dnt', 'განახლება'), ['class' => $model->isNewRecord ? 'btn btn-success btn-block' : 'btn btn-primary btn-block']) ?>
            </div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
