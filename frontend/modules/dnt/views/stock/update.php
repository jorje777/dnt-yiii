<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\modules\dnt\models\Stock */
/* @var $types [] */
/* @var $drugs [] */

$this->title = Yii::t('dnt', 'განაახლე {modelClass}: ', [
    'modelClass' => 'Stock',
]) . $model->id;
?>
<div class="stock-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'drugs' => $drugs,
        'types' => $types,
    ]) ?>

</div>
