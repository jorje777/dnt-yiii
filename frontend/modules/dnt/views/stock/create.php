<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\modules\dnt\models\Stock */
/* @var $types [] */
/* @var $drugs [] */

$this->title = Yii::t('dnt', 'მარაგიში დამატება');

?>
<div class="stock-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'drugs' => $drugs,
        'types' => $types,
    ]) ?>

</div>
