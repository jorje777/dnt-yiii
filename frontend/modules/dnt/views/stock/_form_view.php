<?php
/**
 * Created by PhpStorm.
 * User: sai
 * Date: 8/7/17
 * Time: 5:47 PM
 * @author Saiat Kalbiev <kalbievich11@gmail.com>
 */

use kartik\select2\Select2;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\modules\dnt\models\Stock */
/* @var $form yii\widgets\ActiveForm */
/* @var $types [] */
/* @var $action $string */
?>

<div class="stock-form">

    <?php $form = ActiveForm::begin([
        'action' => \yii\helpers\Url::to(['/dnt/stock/update', 'id' => $model->id])
    ]); ?>

    <tr>
        <td class="col-md-2 text-center" style="vertical-align: middle">
            <span style="font-size: 18px"><?= $model->updated_at ? date('Y-m-d  g:i a', $model->updated_at) : date('Y-m-d  G:i:s', $model->created_at) ?></span>
        </td>
        <td class="col-md-2 text-center" style="vertical-align: middle">
            <?= $form->field($model, 'quantity')->textInput([
                'disabled' => $model->is_used ? true : false
            ])->label(false) ?>
        </td>
        <td class="col-md-2 text-center">
            <?= Html::textInput(null,$model->initial_quantity,[
                'disabled' => true,
                'class' => 'form-control'
            ]) ?>
        </td>
        <td class="col-md-2 text-center" style="vertical-align: middle">
            <?= $form->field($model, 'price')->textInput([
                'maxlength' => true,
                'value' => $model->price,
                'disabled' => $model->is_used ? true : false
            ])->label(false) ?>
        </td>
        <td class="col-md-2 text-center" style="vertical-align: middle">
            <?= $form->field($model, 'type')->dropDownList($types,[
                'disabled' => $model->is_used ? true : false
            ])->label(false) ?>
        </td>
        <td class="col-md-2 text-center" style="vertical-align: middle">
            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? Yii::t('dnt', 'შექმნა ') : Yii::t('dnt', 'განახლება'), [
                    'class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary',
                    'disabled' => $model->is_used ? true : false
                ]) ?>
            </div>
        </td>
    </tr>

    <?php ActiveForm::end(); ?>

</div>
