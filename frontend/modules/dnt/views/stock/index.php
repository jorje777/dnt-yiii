<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\modules\dnt\models\search\StockSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $drugType string */
/* @var $sumPriceOfStock [] */

$this->title = Yii::t('dnt', 'მარაგი') . " : " . \frontend\modules\dnt\models\Drug::getDrugTypes()[$drugType];

?>
<div class="stock-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('dnt', 'მარაგში დამატება'), ['create','drug_type' => $drugType], ['class' => 'btn btn-success']) ?>
        <span class="pull-right" style="font-size: 20px">საერთო ფასი:  <?= $sumPriceOfStock['sum'] ? $sumPriceOfStock['sum'] : 0 ?></span>

    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
//        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'drug_id',
                'value' => function ($model) {
                    return $model->drug->name;
                }
            ],
            'quantity',

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view}',
                'buttons' => [
                    'view' => function ($url) {
                        return Html::a('<span class="glyphicon glyphicon-eye-open" style="display: block;text-align: center"></span>',
                            $url);
                    }
                ],
                'urlCreator' => function ($action, $model) use ($drugType) {
                    if ($action === 'view') {
                        $url ='view?drug_id='.$model->drug_id.'&drug_type='.$drugType;
                        return $url;
                    }
                }
            ],
        ],
    ]); ?>
</div>
