<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\modules\dnt\models\Stock */
/* @var $drugModels [] */
/* @var $drugs [] */
/* @var $types [] */
/* @var $archive integer */
/* @var $drugType integer */
/* @var $drugId integer */

$this->title = count($drugModels) > 0? $drugModels[0]->drug->name : "";
$checkExistence = count($drugModels) > 0;
?>
<div class="stock-view">
    <p>
        <?= Html::a('<span class="glyphicon glyphicon-chevron-left"></span> უკან',['/dnt/stock/index','drug_type' => $drugType],['class' => 'btn btn-default']) ?>
        <?= Html::a('<span class="glyphicon glyphicon-folder-open"></span>  არქივი',['/dnt/stock/view','drug_id' => $drugId ,'drug_type' => $drugType,'archive' => $archive ? 0 : 1],['class' => 'btn btn-default'])?>
    </p>

    <h1><?= Html::encode($this->title) ?></h1>
    <table class="table table-bordered <?= $checkExistence ? '' : 'hide' ?>">
        <tr>
            <th class="col-md-2  text-center">
                <h3>ბოლო განახლება</h3>
            </th>
            <th class="col-md-2  text-center">
                <h3>რაოდენობა</h3>
            </th>
            <th class="col-md-2  text-center">
                <h3>დასაწყისი რაოდენობა</h3>
            </th>
            <th class="col-md-2  text-center">
                <h3>ფასი</h3>
            </th>
            <th class="col-md-2  text-center">
                <h3>ტიპი</h3>
            </th>
            <th class="col-md-2  text-center">
            </th>
        </tr>
    <?php
    /** @var \frontend\modules\dnt\models\Stock $drug */
    if($checkExistence){
        foreach ($drugModels as $drugModel){
            echo $this->render('_form_view', [
                'model' => $drugModel,
                'types' => $types,
            ]);
        }
    }
    ?>
    </table>

</div>
