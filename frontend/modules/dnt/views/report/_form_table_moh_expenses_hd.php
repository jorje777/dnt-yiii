<?php
/**
 * Created by PhpStorm.
 * User: kalbievich
 * Date: 9/16/17
 * Time: 3:27 PM
 * @author Saiat Kalbiev <kalbievich11@gmail.com>
 */

/* @var $expensesHd [] */
?>

<table class="table table-bordered">
    <thead>
    <tr>
        <th>სახელი</th>
        <th>პრეპ. რაოდენობა</th>
        <th>ლაბ</th>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($expensesHd as $expense): ?>
        <?php /** @var \frontend\modules\dnt\models\Expense $expense */ ?>
        <tr>
            <td> <?= $expense['name'] ?> </td>
            <td> <?= $expense['prep']?> </td>
            <td> <?= $expense['lab']?> </td>

        </tr>
    <?php endforeach; ?>
    </tbody>
</table>

