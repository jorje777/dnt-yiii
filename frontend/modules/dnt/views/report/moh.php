<?php
/**
 * Created by PhpStorm.
 * User: kalbievich
 * Date: 9/16/17
 * Time: 2:41 PM
 * @author Saiat Kalbiev <kalbievich11@gmail.com>
 */

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $startDate string */
/* @var $endDate string */
/* @var $expensesPd [] */
/* @var $expensesHd [] */
/* @var $message string */

?>

<?php $form = ActiveForm::begin([
    'action' => '/dnt/report/moh',
    'method' => "POST",
    'id' => 'mohReportForm'
]); ?>

<div class="row">
    <div class="col-md-3">
        <?= \kartik\date\DatePicker::widget([
            'name' => 'start_date',
            'value' => $startDate,
            'options' => ['placeholder' => 'აირჩიე თარიღი ...'],
            'pluginOptions' => [
                'format' => 'yyyy-mm-dd',
                'todayHighlight' => true,
                'autoclose' => true
            ]
        ])?>
    </div>
    <div class="col-md-3">
        <?= \kartik\date\DatePicker::widget([
            'name' => 'end_date',
            'value' => $endDate,
            'options' => ['placeholder' => 'აირჩიე თარიღი ...'],
            'pluginOptions' => [
                'format' => 'yyyy-mm-dd',
                'todayHighlight' => true,
                'autoclose' => true
            ]
        ])?>
    </div>
    <div class="col-md-2">
        <?= Html::submitButton('ფილტრი  ', [
            'class' => 'btn btn-primary btn-block',
            'id' => 'buttonReportMohDefault'
        ]) ?>
    </div>
    <div class="col-md-2">
        <?= Html::submitButton('ჰდ რეპორტი', [
            'class' => 'btn btn-success btn-block',
            'id' => 'excelButtonReportMohHd'
        ]) ?>
    </div>
    <div class="col-md-2">
        <?= Html::submitButton('პდ რეპორტი', [
            'class' => 'btn btn-success btn-block',
            'id' => 'excelButtonReportMohPd'
        ]) ?>
    </div>
</div>

<?php ActiveForm::end(); ?>
<br>
<div class="row">
    <div class="col-md-6">
        <?php
        if($expensesHd){
            echo $this->render('_form_table_moh_expenses_hd', [
                'expensesHd' => $expensesHd
            ]);
        }
        ?>
    </div>
    <div class="col-md-6">
        <?php
        if($expensesPd){
            echo $this->render('_form_table_moh_expenses_pd', [
                'expensesPd' => $expensesPd
            ]);
        }
        ?>
    </div>
</div>

