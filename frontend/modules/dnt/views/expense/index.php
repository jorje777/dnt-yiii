<?php
/**
 * Created by PhpStorm.
 * User: sai
 * Date: 8/9/17
 * Time: 11:51 AM
 * @author Saiat Kalbiev <kalbievich11@gmail.com>
 */

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $drugs [] */
/* @var $patients [] */
/* @var $this \yii\web\View */
/* @var $message string */
/* @var $type string */


?>


<div class="alert alert-<?= $type ? $type : " hide" ?>" role="alert" id="generalAlertBox">
    <strong><?= $message ? $message : "" ?></strong>
</div>

<?php $form = ActiveForm::begin(['action' => '/dnt/expense/index', 'method' => 'POST']); ?>
<div class="row">
    <div class="col-md-3 col-md-offset-3">
        <label>გვარი, სახელი</label>
        <?= \kartik\select2\Select2::widget([
            'data' => $patients,
            'name' => 'patient',
            'options' => [
                'placeholder' => 'აიჩიე პაციენტი..',
                'required' => true,
                'class' => 'form-control',
                'id' => 'stockPagePatientNameField'
            ],
            'pluginOptions' => [
                'allowClear' => true,
            ]
        ]) ?>
    </div>
    <div class="col-md-3">
        <label>სეანსის თაროღი</label>
        <?= \kartik\date\DatePicker::widget([
            'name' => 'session_date',
            'value' => date('Y-m-d', time()),
            'options' => ['placeholder' => 'აირჩიე სეანსის თარიღი'],
            'pluginOptions' => [
                'format' => 'yyyy-mm-dd',
                'todayHighlight' => true,
                'autoclose' => true
            ]
        ]) ?>
    </div>
</div>
<br><br>
<div class="row">
    <div class="col-md-6">
        <label>პრეპარატები</label>
        <?= \kartik\select2\Select2::widget([
            'data' => $drugs,
            'name' => 'drug[]',
            'options' => [
                'placeholder' => 'აირჩიე პრეპარატები..',
                'id' => "drug_id",
                'required' => false
            ],
            'pluginOptions' => [
                'allowClear' => true,
                'multiple' => true,
            ]
        ]) ?>
    </div>
</div>
<br><br>
<div id="drugsGenerateDiv">
</div>
<br><br>


<h3>ლაბორატორიული კვლევა</h3>
<span class="btn btn-primary" id="generateLab">
        დამატება
    </span>
<span class="btn btn-danger" id="removeLab">
        წაწლა
    </span>
<br><br>
<div id="labGenerateDiv">

</div>
<br><br>


<h3>სისძარღვოვანი მიდგომა</h3>
<span class="btn btn-primary" id="generateBlood">
            დამატება
        </span>
<span class="btn btn-danger" id="removeBlood">
            წაწლა
        </span>
<br><br>
<div id="bloodGenerateDiv">

</div>
<br><br>

<div class="row">
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <?= Html::textarea('comment', null, [
                'class' => 'form-control',
                'placeholder' => "კომენტარი",
                'rows' => 5
            ]) ?>
        </div>
    </div>
</div>
<br><br>
<div class="row">
    <div class="col-md-6 col-md-offset-3" style="text-align: center">
        <?= Html::submitButton('დამატება', [
            'class' => 'btn btn-default btn-lg',
            'id' => 'stockPageSubmitButton'
        ]) ?>
    </div>
</div>


<?php ActiveForm::end(); ?>
