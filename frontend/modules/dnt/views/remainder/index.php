<?php
/**
 * Created by PhpStorm.
 * User: sai
 * Date: 8/16/17
 * Time: 4:08 PM
 * @author Saiat Kalbiev <kalbievich11@gmail.com>
 */

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $stockData [] */
/* @var $drugTypes [] */
/* @var $type string */
/* @var $message string */

?>

<?php $form = ActiveForm::begin([
    'action' => '/dnt/remainder/index',
    'method' => "POST",
]); ?>

<div class="alert alert-<?= $type ? $type : " hide"?>" role="alert" id="generalAlertBox">
    <strong><?= $message ? $message : "" ?></strong>
</div>

<div class="row">
    <div class="col-md-3">
        <?= Html::dropDownList('drug_type',0,$drugTypes,[
            'class' => 'form-control',
            'id' => 'remainderDrugTypeDropdown',
            'required' => true
        ]) ?>
    </div>
    <div class="col-md-3">
        <?= \kartik\date\DatePicker::widget([
            'name' => 'date',
            'options' => [
                'placeholder' => 'აირჩიე თარიღი ...',
                'required' => true

            ],
            'pluginOptions' => [
                'format' => 'yyyy-mm',
                'todayHighlight' => true,
                'minViewMode' => 1,
                'autoclose' => true
            ]
        ])?>
    </div>
    <div class="col-md-1" style="text-align: center">
        <?= Html::submitButton('დამატება  ', [
            'class' => 'btn btn-primary',
        ]) ?>
    </div>
</div>

<?php ActiveForm::end(); ?>
<br><br>
<div>
    <?php
    if($stockData){
        echo $this->render('_form_table', [
            'stockData' => $stockData
        ]);
    }
    ?>
</div>
