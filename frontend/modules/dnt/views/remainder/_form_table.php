<?php
/**
 * Created by PhpStorm.
 * User: sai
 * Date: 8/16/17
 * Time: 4:41 PM
 * @author Saiat Kalbiev <kalbievich11@gmail.com>
 */

/* @var $stockData [] */
?>
<table class="table table-bordered">
    <thead>
    <tr>
        <th>პრეპარატი</th>
        <th>რაოდენობა</th>
    </tr>
    </thead>
    <tbody  id="remainderStockTableBody">
    <?php /** @var \frontend\modules\dnt\models\Expense $exp */
    $sum = 0;
    foreach ($stockData as $data) { ?>
        <tr>
            <td><?= $data['name'] ?></td>
            <?php
                $sum += $data['sum_price'];
                echo "<td>".$data['quantity']."</td>";
            ?>
        </tr>
    <?php } ?>
    <tr>
        <td></td>
        <td> ჯამი : <?= $sum ?></td>
    </tr>
    </tbody>
</table>

