<?php
/**
 * Created by PhpStorm.
 * User: kalbievich
 * Date: 8/19/17
 * Time: 7:25 PM
 * @author Saiat Kalbiev <kalbievich11@gmail.com>
 */

use yii\helpers\Html;

/* @var $drugs [] */
?>


<div class="row" id="protocolDrugCheckboxColumns">
    <div class="col-md-4">
        <?php
            foreach ($drugs[1] as $categoryName => $theDrugs){
                echo "<h5><b>$categoryName</b>  </h5>";
                foreach ($theDrugs as $drug){
                    echo "<div class='form-group'>";
                    echo Html::checkbox(null,false,[
                        'value' => $drug->id
                    ]);
                    echo " <label style='font-weight: 100'>".$drug->name."</label>";
                    echo "</div>";
                }
            }
        ?>
    </div>
    <div class="col-md-4">
        <?php
            foreach ($drugs[2] as $categoryName => $theDrugs){
                echo "<h5><b>$categoryName</b>  </h5>";
                foreach ($theDrugs as $drug){
                    echo "<div class='form-group'>";
                    echo "</div>";
                    echo Html::checkbox(null,false,[
                        'value' => $drug->id
                    ]);
                    echo " <label style='font-weight: 100'>".$drug->name."</label>";
                }
            }
        ?>
    </div>
    <div class="col-md-4">
        <?php
            foreach ($drugs[3] as $categoryName => $theDrugs){
                echo "<h5><b>$categoryName</b>  </h5>";
                foreach ($theDrugs as $drug){
                    echo "<div class='form-group'>";
                    echo "</div>";
                    echo Html::checkbox(null,false,[
                        'value' => $drug->id
                    ]);
                    echo " <label style='font-weight: 100'>".$drug->name."</label>";
                }
            }
        ?>
    </div>
</div>
