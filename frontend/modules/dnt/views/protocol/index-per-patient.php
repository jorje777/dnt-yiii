<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\modules\dnt\models\search\ProtocolSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $type integer */

$this->title = Yii::t('dnt', 'Protocols');

?>
<div class="row">
    <div class="col-md-1">
        <?= Html::a('<span class="glyphicon glyphicon-chevron-left"></span> უკან',['/dnt/protocol/index','type' => $type],['class' => 'btn btn-default']) ?>
    </div>
    <div class="col-md-1">
        <?= Html::submitButton(Yii::t('dnt', 'Print '), [
            'class' => 'btn btn-info btn-block',
            'style' => 'margin-bottom:10px',
            'id' => 'protocolPrintButton'
        ])?>
    </div>
</div>

<div class="protocol-index">
    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
//        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'patient_id',
                'value' => /**
                 * @param $model \frontend\modules\dnt\models\Protocol
                 * @return mixed
                 * @author Saiat Kalbiev <kalbievich11@gmail.com>
                 */
                    function ($model) {
                        return $model->patient->getFullName();
                    }
            ],
            [
                'attribute' => 'session_date',
                'value' => function($model){
                    return date("y-m-d",$model->session_date);
                }
            ],
            [
                'attribute' => 'session_start_time',
            ],
            [
                'attribute' => 'session_end_time',
            ],
            [
                'attribute' => 'doctor_id',
                'value' => function($model){
                    return $model->doctor ? $model->doctor->getFullName() : "";
                }
            ],
            [
                'attribute' => 'initial_weight',
                'value' => function($model){
                    return $model->initial_weight . " - " . $model->end_weight;
                }

            ],
            [
                'attribute' => 'initial_pressure',
                'value' => function($model){
                    return $model->initial_pressure . " - " . $model->end_pressure;
                }
            ],
            [
                'attribute' => 'ultrafiltrates',
                'label' => 'უ.ფილტრატი'
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{delete}{update}',
                'buttons' => [
                    'view' => function ($url) {
                        return Html::a('<span class="glyphicon glyphicon-eye-open" style="display: block;text-align: center"></span>',
                            $url);
                    }
                ],
                'urlCreator' => function ($action, $model) use ($type) {
                    if ($action === 'update') {
                        $url ='update?id='.$model->id.'&type='.$type;
                        return $url;
                    }else if($action === 'delete'){
                        $url ='delete?id='.$model->id.'&type='.$type;
                        return $url;
                    }
                }
            ],
        ],
    ]); ?>
</div>

<iframe style="width: 100%;height: 100%;visibility: hidden" id="customIframe"></iframe>
