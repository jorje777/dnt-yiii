<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\modules\dnt\models\search\ProtocolSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $type integer */
/* @var $patient_types [] */
/* @var $patients [] */
/* @var $startDate [] */

$this->title = Yii::t('dnt', 'პროტოკოლი : ') . \frontend\modules\dnt\models\Protocol::getProtocolTypes()[$type];;

?>


<div class="row">
    <div class="col-md-3">
        <?= $form = Html::beginForm('/dnt/protocol/index', 'GET'); ?>
        <?= \kartik\select2\Select2::widget([
            'data' => $patients,
            'name' => 'patient_id',
            'options' => [
                'placeholder' => 'აირჩიე პაციენტი..',
            ],
            'pluginOptions' => [
                'allowClear' => true,
                'readonly' => true
            ]
        ]) ?>
    </div>
    <div class="col-md-2">
        <?= Html::dropDownList('patient_type', null, $patient_types, [
            'class' => 'form-control'
        ]) ?>
    </div>
    <div class="col-md-3">
        <?= \kartik\date\DatePicker::widget([
            'name' => 'start_date',
            'value' => $startDate,
            'options' => ['placeholder' => 'აირჩიე თარიღი ...'],
            'pluginOptions' => [
                'format' => 'yyyy-mm-dd',
                'todayHighlight' => true,
                'autoclose' => true
            ]
        ]) ?>
    </div>
    <div class="col-md-3">
        <?= \kartik\date\DatePicker::widget([
            'name' => 'end_date',
            'value' => $endDate,
            'options' => ['placeholder' => 'აირჩიე თარიღი ...'],
            'pluginOptions' => [
                'format' => 'yyyy-mm-dd',
                'todayHighlight' => true,
                'autoclose' => true
            ]
        ]) ?>
    </div>
    <?= Html::hiddenInput('type', $type) ?>
    <div class="col-md-1">
        <?= Html::submitButton(Yii::t('dnt', 'ძებნა '), [
            'class' => 'btn btn-success btn-block',
        ]) ?>
    </div>
    <?= Html::endForm(); ?>
</div>

<div class="row">
    <div class="col-md-11">
        <h1><?= Html::encode($this->title) ?></h1>
    </div>
</div>


<div class="protocol-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
//        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'patient_id',
                'value' => /**
                 * @param $model \frontend\modules\dnt\models\Protocol
                 * @return mixed
                 * @author Saiat Kalbiev <kalbievich11@gmail.com>
                 */
                    function ($model) {
                        return $model->patient->getFullName();
                    }
            ],
            [
                'attribute' => 'session_date',
                'value' => function ($model) {
                    return date("y-m-d", $model->session_date);
                }
            ],
            [
                'attribute' => 'initial_weight',
                'value' => function ($model) {
                    return $model->initial_weight . " - " . $model->end_weight;
                }

            ],
            [
                'attribute' => 'initial_pressure',
                'value' => function ($model) {
                    return $model->initial_pressure . " - " . $model->end_pressure;
                }
            ],
            [
                'attribute' => 'ultrafiltrates',
                'label' => 'უ.ფილტრატი'
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view}',
                'buttons' => [
                    'view' => function ($url, $model) use ($type,$startDate,$endDate) {
                        $urlConfig = [
                            '/dnt/protocol/view-per-patient',
                            'patient_id' => $model->patient_id,
                            'type' => $type
                        ];
                        if($startDate) {
                            $urlConfig = array_merge($urlConfig,[
                                'start_date' => $startDate
                            ]);
                        }
                        if($endDate) {
                            $urlConfig = array_merge($urlConfig,[
                                'end_date' => $endDate
                            ]);
                        }
                        return Html::a('<span class="glyphicon glyphicon-eye-open" style="display: block;text-align: center"></span>', \yii\helpers\Url::to($urlConfig));
                    }
                ]
            ],
        ],
    ]); ?>

</div>
