<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\modules\dnt\models\Protocol */
/* @var $patients [] */
/* @var $drugs [] */
/* @var $type integer */
/* @var $message string */
/* @var $messageType string */

$this->title = Yii::t('dnt', 'პროტოკოლის შექმნა : '). \frontend\modules\dnt\models\Protocol::getProtocolTypes()[$type];
?>
<div class="protocol-create">

    <h1><?= Html::encode($this->title) ?></h1>
    <br>

    <div class="alert alert-<?= $messageType ? $messageType : " hide"?>" role="alert" id="generalAlertBox">
        <strong><?= $message ? $message : "" ?></strong>
    </div>

    <?= $this->render('_form', [
        'model' => $model,
        'patients' => $patients,
        'type' => $type,
        'drugs' => $drugs
    ]) ?>

</div>
