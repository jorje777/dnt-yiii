<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\modules\dnt\models\Protocol */


?>
<div class="protocol-view">


    <p>
        <?= Html::a(Yii::t('dnt', 'განახლება'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('dnt', 'წაშლა'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('dnt', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'attribute' => 'patient_id',
                'value' => function($model){
                    return $model->patient->getFullName();
                }
            ],
            [
                'attribute' => 'session_date',
                "value" => function ($model) {
                    return date('y-m-d',$model->session_date);
                }
            ],
            'session_start_time',
            'session_end_time',
            [
                'attribute' => 'initial_weight',
                'label' => 'წონა (სეანსის დასაწყისი)'
            ],
            [
                'attribute' => 'end_weight',
                'label' => 'წონა (სეანსის დასასრილი)'
            ],
            [
                'attribute' => 'initial_pressure',
                'label' => 'წნევა (სეანსის დასაწყისი)'
            ],
            [
                'attribute' => 'end_pressure',
                'label' => 'წნევა (სეანსის დასასრილი)'
            ],
            'ultrafiltrates'
        ],
    ]) ?>

</div>
