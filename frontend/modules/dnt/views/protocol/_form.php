<?php

use frontend\modules\dnt\models\Doctor;
use kartik\select2\Select2;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\modules\dnt\models\Protocol */
/* @var $form yii\widgets\ActiveForm */
/* @var $patients [] */
/* @var $drugs [] */
/* @var $type integer */
/* @var $expenseDrugs [] */

?>

<div class="protocol-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-sm-offset-2 col-sm-4    ">
            <?= Html::activeHiddenInput($model, 'type', [
                'value' => $type
            ]) ?>
            <?= $form->field($model, 'patient_id')->widget(Select2::classname(), [
                'data' => $patients,
                'options' => [
                    'placeholder' => 'აირჩიე პაციენტი..',
                ],
                'disabled' => $model->patient_id ? true : false,
                'pluginOptions' => [
                    'allowClear' => true,
                    'readonly' => true
                ],
            ]) ?>
            <?php
            if (isset($model->patient_id)) {
                echo Html::activeHiddenInput($model, 'patient_id');
            }
            ?>
        </div>
        <div class="col-sm-4">
            <?= $form->field($model, 'doctor_id')->widget(Select2::classname(), [
                'data' => Doctor::getDoctorsMapped(),
                'options' => [
                    'placeholder' => 'აირჩიე ექთანი..',
                ],
                'disabled' => false,
                'pluginOptions' => [
                    'allowClear' => true,
                    'readonly' => true
                ],
            ]) ?>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-sm-3">
            <?= $form->field($model, 'session_date')->widget(\kartik\date\DatePicker::className(), [
                'options' => [
                    'placeholder' => 'სეანსის თარიღი',
                    'value' => $model->session_date ? date('Y-m-d', $model->session_date) : null,
                ],
                'pluginOptions' => [
                    'format' => 'yyyy-m-dd',
                    'todayHighlight' => true,
                    'autoclose' => true
                ]
            ]) ?>
            <?= Html::checkbox(null, false, [
                'id' => 'protocolTimeCheckBox',
            ]) ?> <label>სეანსის დრო</label>
            <div class="hide" id="protocolTimeDiv">
                <?= $form->field($model, 'session_start_time')->widget(\kartik\time\TimePicker::className(), [
                    "options" => [
                        'value' => "",
                    ],
                    'pluginOptions' => [
                        'showSeconds' => false,
                        'showMeridian' => false,
                        'minuteStep' => 1,
                        'defaultTime' => $model->session_start_time ? $model->session_start_time : false
                    ]
                ]); ?>
                <?= $form->field($model, 'session_end_time')->widget(\kartik\time\TimePicker::className(), [
                    "options" => [
                        'value' => "",
                    ],
                    'pluginOptions' => [
                        'showSeconds' => false,
                        'showMeridian' => false,
                        'minuteStep' => 1,
                        'defaultTime' => $model->session_end_time ? $model->session_end_time : false
                    ]
                ]); ?>
            </div>
        </div>
        <div class="col-sm-3">
            <?= $form->field($model, 'initial_weight')->textInput()->label('წონა (სეანსის დასაწყისი)') ?>
            <?= $form->field($model, 'end_weight')->textInput()->label('წონა (სეანსის დასასრილი)') ?>
        </div>
        <div class="col-sm-3">
            <?= $form->field($model, 'initial_pressure')->textInput(['maxlength' => true])->label('წნევა (სეანსის დასაწყისი)') ?>
            <?= $form->field($model, 'end_pressure')->textInput(['maxlength' => true])->label('წნევა (სეანსის დასასრილი)') ?>
        </div>
        <div class="col-sm-3">
            <?= $form->field($model, 'ultrafiltrates')->textInput(['maxlength' => true]) ?>
        </div>
    </div>
    <br>
    <br>
    <?php
    if (count($drugs) > 0) {
        echo $this->render('_drug_columns', [
            'drugs' => $drugs
        ]);
    }
    ?>

    <br>
    <br>
    <div class="col-md-4 col-md-offset-4">
        <?php Modal::begin([
            'header' => '<table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>სახელი</th>
                                    <th>რაოდენობა</th>
                                    <th>მარაგის რაოდენობა</th>
                                </tr>
                            </thead>
                            <tbody id="protocolModalTableBody">
                            </tbody>
                          </table>',
            'toggleButton' => [
                'label' => 'შექმნა',
                'class' => 'btn btn-success btn-block',
                'id' => 'reportModalBtn'
            ],
            'options' => [
                'id' => 'protocolModal'
            ]
        ]); ?>
        <div class="row">
            <div class="form-group col-md-4 col-md-offset-4">
                <?= Html::submitButton(Yii::t('dnt', 'შექმნა '), [
                    'class' => 'btn btn-success btn-block',
                    'id' => 'protocolSubmitButton',
                    'data' => [
                        'confirm' => 'დავადასტუროთ თქვენი მოტხოვნა?'
                    ]
                ]) ?>
            </div>
        </div>

        <?php Modal::end(); ?>
    </div>

    <?php
    if (isset($expenseDrugs) && count($expenseDrugs) > 0) {
        echo "<h2>დახარჯული</h2>";
        echo $this->render('_form_table_expense_drugs', [
            'expenseDrugs' => $expenseDrugs
        ]);
    }
    ?>

    <?php ActiveForm::end(); ?>

</div>
