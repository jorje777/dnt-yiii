<?php
/**
 * Created by PhpStorm.
 * User: kalbievich
 * Date: 8/20/17
 * Time: 11:28 AM
 * @author Saiat Kalbiev <kalbievich11@gmail.com>
 */
/* @var $expenseDrugs [] */

?>

<table class="table table-bordered">
    <thead>
    <tr>
        <th>პრეპარატი</th>
        <th>რაოდენობა</th>
        <th>ფასი</th>
    </tr>
    </thead>
    <tbody>
    <?php /** @var \frontend\modules\dnt\models\Expense $expenseDrug */
    foreach ($expenseDrugs as $expenseDrug): ?>
        <tr>
            <td><?= $expenseDrug->stock->drug->name ?></td>
            <td><?= $expenseDrug->quantity ?></td>
            <td><?= $expenseDrug->stock->price ?></td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>

