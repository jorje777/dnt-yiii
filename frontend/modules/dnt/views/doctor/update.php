<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\modules\dnt\models\Doctor */

$this->title = 'Update Doctor: ' . ' ' . $model->name;
?>
<div class="doctor-update">

    <?php echo $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
