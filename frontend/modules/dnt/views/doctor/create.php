<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\modules\dnt\models\Doctor */

$this->title = 'Create Doctor';
?>
<div class="doctor-create">

    <?php echo $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
