<?php
/**
 * Created by PhpStorm.
 * User: sai
 * Date: 8/15/17
 * Time: 12:08 PM
 * @author Saiat Kalbiev <kalbievich11@gmail.com>
 */
/* @var $expense [] */

$counter = 1;

?>

<table class="table table-bordered">
    <thead>
    <tr>
        <th>#</th>
        <th>პრეპარატი</th>
        <th>სეანსის თარიღი</th>
        <th>რაოდენობა</th>
        <th>კომენტარი</th>
    </tr>
    </thead>
    <tbody>
    <?php /** @var \frontend\modules\dnt\models\Expense $exp */
    foreach ($expense as $exp): ?>
        <tr>
            <td><?= $counter++ ?></td>
            <td><?= $exp->stock->drug->name ?></td>
            <td><?= date('Y-m-d  g:i a',$exp->session_date) ?></td>
            <td><?= $exp->quantity ?></td>
            <td><?= $exp->comment ?></td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>
