<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\modules\dnt\models\Patient */
/* @var $groups []*/
/* @var $expense []*/

$this->title = Yii::t('dnt', 'განაახლე {modelClass}: ', [
    'modelClass' => 'Patient',
]) . $model->name;
?>
<div class="patient-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'groups' => $groups,
        'expense' => $expense
    ]) ?>

</div>
