<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\modules\dnt\models\Patient */
/* @var $groups [] */

$this->title = Yii::t('dnt', 'პაციენტის შექმნა');

?>
<div class="patient-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'groups' => $groups
    ]) ?>

</div>
