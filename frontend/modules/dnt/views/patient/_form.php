<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\modules\dnt\models\Patient */
/* @var $form yii\widgets\ActiveForm */
/* @var $groups [] */
/* @var $expense [] */

?>

<div class="patient-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'surname')->textInput(['maxlength' => true]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'personal_id')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'birth_date')->widget(\kartik\date\DatePicker::className(),[
                'options' => [
                    'placeholder' => 'აირჩიე თარიღი ...',
                    'value' => date("Y-m-d",51523324)
                ],
                'pluginOptions' => [
                    'format' => 'yyyy-m-dd',
                    'todayHighlight' => true,
                    'autoclose' => true

                ]
            ])?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'group')->dropDownList($groups)?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'transplant_date')->widget(\kartik\date\DatePicker::className(),[
                'options' => ['placeholder' => 'აირჩიე თარიღი ....'],
                'pluginOptions' => [
                    'format' => 'yyyy-m-dd',
                    'todayHighlight' => true,
                    'autoclose' => true

                ]
            ]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'group_rezus')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'mors')->widget(\kartik\date\DatePicker::className(),[
                'options' => ['placeholder' => 'აირჩიე თარიღი ...'],
                'pluginOptions' => [
                    'format' => 'yyyy-m-dd',
                    'todayHighlight' => true,
                    'autoclose' => true
                ]
            ]) ?>
        </div>
    </div>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('dnt', 'შექმნა') : Yii::t('dnt', 'განახლება'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
    <?php ActiveForm::end(); ?>


    <?php
        if(isset($expense)){
            echo $this->render('_form_table_expense', [
                'expense' => $expense
            ]);
        }
    ?>
</div>
