<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\modules\dnt\models\search\PatientSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $patientGroups [] */

$this->title = Yii::t('dnt', 'პაციენტები');

?>
<div class="patient-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'name',
            'surname',
            'personal_id',
            'birth_date',
            [
                'attribute' => 'group',
                'filter' => Html::activeDropDownList($searchModel,'group',$patientGroups,[
                    'class' => 'form-control'
                ])
            ],
            'transplant_date',
            'mors',
            'group_rezus',

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{delete}  {update}'
            ],
        ],
    ]); ?>
</div>
