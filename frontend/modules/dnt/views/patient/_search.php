<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\modules\dnt\models\search\PatientSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="patient-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'name') ?>

    <?= $form->field($model, 'surname') ?>

    <?= $form->field($model, 'personal_id') ?>

    <?= $form->field($model, 'birth_date') ?>

    <?php // echo $form->field($model, 'group') ?>

    <?php // echo $form->field($model, 'transplant_date') ?>

    <?php // echo $form->field($model, 'mors') ?>

    <?php // echo $form->field($model, 'group_rezus') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('dnt', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('dnt', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
