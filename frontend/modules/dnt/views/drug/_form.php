<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\modules\dnt\models\Drug */
/* @var $form yii\widgets\ActiveForm */
/* @var $drugCategories [] */
/* @var $drugType integer */

?>

<div class="drug-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-md-3">
            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'drug_type')->hiddenInput([
                'value' => $drugType
            ])->label(false) ?>
        </div>
        <?php if (isset($drugCategories)) :?>
            <div class="col-md-3">
                <?= $form->field($model, 'category_id')->dropDownList($drugCategories); ?>
            </div>
        <?php  endif; ?>
        <div class="col-md-2" style="padding-top: 23px">
                <?= Html::submitButton($model->isNewRecord ? Yii::t('dnt', 'შექმნა') : Yii::t('dnt', 'განახლება'), ['class' => $model->isNewRecord ? 'btn btn-success btn-block' : 'btn btn-primary btn-block']) ?>
        </div>
    </div>







    <?php ActiveForm::end(); ?>

</div>
