<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\modules\dnt\models\Drug */
/* @var $drugCategories [] */
/* @var $drugType integer*/


$this->title = Yii::t('dnt', ' შეცვალე: ', [
    'modelClass' => 'Drug',
]) . $model->name;
?>
<div class="drug-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'drugType' => $drugType,
        'drugCategories' => $drugCategories
    ]) ?>

</div>
