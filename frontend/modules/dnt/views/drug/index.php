<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\modules\dnt\models\search\DrugSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $drugTypes [] */
/* @var $drugCategories [] */
/* @var $drugType integer */
/* @var $model \frontend\modules\dnt\models\Drug  */
/* @var $type string  */
/* @var $message string  */

$this->title = Yii::t('dnt', 'პრეპარატები');

?>
<div class="drug-index">

    <div class="alert alert-<?= $type ? $type : " hide"?>" role="alert" id="generalAlertBox">
        <strong><?= $message ? $message : "" ?></strong>
    </div>

    <?= $this->render('_form', [
        'model' => $model,
        'drugType' => $drugType,
        'drugCategories' => $drugCategories
    ]) ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'name',
            [
                'attribute' => 'drug_type',
                'value' => function ($model) use ($drugTypes) {
                    return $drugTypes[$model->drug_type];
                }
            ],
            [
                'attribute' => 'category_id',
                'value' => function ($model) {
                    if(isset($model->category->name)){
                        return $model->category->name;
                    }
                    return "No category";
                }
            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => "{update}{delete}"
            ],
        ],
    ]); ?>
</div>
