<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\modules\dnt\models\DrugCategory */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="drug-category-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'is_mwvave')->dropDownList([
        '0' => "Not Mwvave",
        '1' => "Is Mwvave"
    ]) ?>

    <?= $form->field($model,'protocol_col_index') ->dropDownList([
        1 => 1,
        2 => 2,
        3 => 3
    ])?>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('dnt', 'შექმნა') : Yii::t('dnt', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
