<?php
/**
 * Created by PhpStorm.
 * User: kalbievich
 * Date: 8/13/17
 * Time: 6:48 PM
 */

use frontend\modules\dnt\models\Stock;

/* @var $out [] */
/* @var $sumPrice integer */

$counter = 1;

?>

<table class="table table-bordered">
    <thead>
        <tr>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th><?= $sumPrice ?></th>
            <th></th>
        </tr>
        <tr>
            <th>#</th>
            <th>სეანსის თარიღი</th>
            <th>სახელი</th>
            <th>პრეპარატის სახელი</th>
            <th>რაოდენობა</th>
            <th>ტიპი</th>
            <th>ერთეულის ფასი</th>
            <th>საერთო ფასი</th>
            <th>კომენტარი</th>
        </tr>
    </thead>
    <tbody>
    <?php /** @var \frontend\modules\dnt\models\Expense $expense */
        foreach ($out as $expense): ?>
            <tr>
                <td><?= $counter++ ?></td>
                <td><?= date("Y-m-d",$expense->session_date) ?></td>
                <td><?= $expense->patient->getFullName() ?></td>
                <td><?= $expense->stock->drug->name ?></td>
                <td><?= $expense->quantity ?></td>
                <td><?= $expense->stock->type ? Stock::getQuantityTypeConstants()[$expense->stock->type ] : "" ?></td>
                <td><?= $expense->stock->price ?></td>
                <td><?= number_format($expense->stock->price * $expense->quantity,7) ?></td>
                <td><?= $expense->comment ?></td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>
