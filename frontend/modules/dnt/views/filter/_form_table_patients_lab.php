<?php
/**
 * Created by PhpStorm.
 * User: kalbievich
 * Date: 9/23/17
 * Time: 1:48 PM
 * @author Saiat Kalbiev <kalbievich11@gmail.com>
 */

/* @var $labs [] */
$sumPrice = 0;
$sumQuantity = 0;

foreach($labs as $lab){
    $sumPrice += $lab->price * $lab->quantity;
    $sumQuantity += $lab->quantity;
}

?>


<table class="table table-bordered">
    <thead>
    <tr>
        <td></td>
        <td></td>
        <td><?= $sumQuantity ?></td>
        <td><?= $sumPrice ?></td>
        <td></td>
    </tr>
    <tr>
        <th>გვარი, სახელი</th>
        <th>სახელი</th>
        <th>რაოდენობა</th>
        <th>ფასი</th>
        <th>სეანსის თარიღი</th>
    </tr>
    </thead>
    <tbody>
    <?php
    /** @var \frontend\modules\dnt\models\Lab $lab */
    foreach ($labs as $lab): ?>
        <tr>
            <td><?= $lab->patient->getFullName() ?></td>
            <td><?= $lab->name ?></td>
            <td><?= $lab->quantity?></td>
            <td><?= $lab->price?></td>
            <td><?= date("Y-m-d",$lab->session_date)?></td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>
