<?php
/**
 * Created by PhpStorm.
 * User: sai
 * Date: 8/21/17
 * Time: 12:06 PM
 * @author Saiat Kalbiev <kalbievich11@gmail.com>
 */

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $startDate  string */
/* @var $endDate string  */
/* @var $expenseData [] */
/* @var $drugs [] */
/* @var $drug_types [] */
/* @var $patients [] */
/* @var $selectedPatient string */
/* @var $selectedDrug string */

?>
<?php $form = ActiveForm::begin([
    'action' => '/dnt/filter/moh',
    'method' => "POST",
    'id' => 'mohFilterForm'
]); ?>
<div class="row">
    <div class="col-md-4 col-md-offset-4">
        <label>დასაწყისი თარიღი</label>
        <?= \kartik\date\DatePicker::widget([
            'name' => 'start_date',
            'value' => $startDate,
            'options' => ['placeholder' => 'აირჩიე თარიღი ...'],
            'pluginOptions' => [
                'format' => 'yyyy-mm-dd',
                'todayHighlight' => true,
                'autoclose' => true
            ]
        ])?>
    </div>
</div>
<br>
<div class="row">
    <div class="col-md-4 col-md-offset-4">
        <label>ბოლო თარიღი</label>
        <?= \kartik\date\DatePicker::widget([
            'name' => 'end_date',
            'value' => $endDate,
            'options' => ['placeholder' => 'აირჩიე თარიღი ...'],
            'pluginOptions' => [
                'format' => 'yyyy-mm-dd',
                'todayHighlight' => true,
                'autoclose' => true
            ]
        ])?>
    </div>
</div>
<br>






<div class="row">
    <div class="col-md-4 col-md-offset-4">
        <?= \kartik\select2\Select2::widget([
            'data' => $patients,
            'name' => 'patient_id',
            'value' => $selectedPatient ? $selectedPatient : null ,
            'options' => [
                'placeholder' => 'აირჩიე პაციენტი ...',
            ],
            'pluginOptions' => [
                'allowClear' => true,
            ]
        ]) ?>
    </div>
</div>
<br>
<div class="row">
    <div class="col-md-4 col-md-offset-4">
        <label>Category</label>
        <?= \yii\helpers\Html::dropDownList('drug_type',$selectedDrug,$drug_types,[
            'id' => 'filterDrugType',
            'class' => 'form-control'
        ]) ?>
    </div>
</div>
<br>







<div class="row">
    <div class="col-md-4 col-md-offset-4" style="text-align: center">
        <?= Html::submitButton('Filter  ', [
            'class' => 'btn btn-primary btn-block',
            'id' => 'excelButtonMohDefault'
        ]) ?>
    </div>
</div>
<br>
<div class="row <?= isset($expenseData) ? " " : " hide" ?>">
    <div class="col-md-4 col-md-offset-4" style="text-align: center">
        <?= Html::submitButton('Excel', [
            'class' => 'btn btn-success btn-block',
            'id' => 'excelButtonMoh'
        ]) ?>
    </div>
</div>
<br>

<?php
if(isset($expenseData) && count($expenseData) > 0){
    echo $this->render('_form_table_moh_data', [
        'expenseData' => $expenseData,
        'drugs' => $drugs
    ]);
}
?>
<?php ActiveForm::end(); ?>
