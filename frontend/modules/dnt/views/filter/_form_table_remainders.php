<?php
/**
 * Created by PhpStorm.
 * User: sai
 * Date: 8/16/17
 * Time: 4:41 PM
 * @author Saiat Kalbiev <kalbievich11@gmail.com>
 */

use frontend\modules\dnt\models\Stock;

/* @var $remainders [] */
?>
<table class="table table-bordered">
    <thead>
    <tr>
        <th>პრეპარატი</th>
        <th>რაოდენობა</th>
        <th>ტიპი</th>
    </tr>
    </thead>
    <tbody  id="remainderStockTableBody">
    <?php
    $sum = 0;
    /** @var \frontend\modules\dnt\models\Remainder $remainder */
    foreach ($remainders as $remainder): ?>
        <tr>
            <td><?= $remainder->drug_name ?></td>
            <td><?= $remainder->quantity ?></td>
            <td><?= $remainder->stock_type ? Stock::getQuantityTypeConstants()[$remainder->stock_type] : ""?></td>
            <?php $sum += $remainder->sum_price ?>
        </tr>
    <?php endforeach; ?>
    <tr>
        <td></td>
        <td><?= 'ჯამი '. $sum ?></td>
        <td></td>
    </tr>
    </tbody>
</table>

