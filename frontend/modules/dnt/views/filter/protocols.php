<?php
/**
 * Created by PhpStorm.
 * User: kalbievich
 * Date: 8/14/17
 * Time: 10:09 PM
 */


use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $protocols [] */
/* @var $protocolTypes [] */
/* @var $startDate integer */
/* @var $endDate integer */
/* @var $selectedProtocol string */


?>

<?php $form = ActiveForm::begin([
    'action' => '/dnt/filter/protocols',
    'method' => "POST",
    'id' => "protocolsFilterForm"
]); ?>

<div class="row">
    <div class="col-md-4 col-md-offset-4">
        <label>დასაწყისი თარიღი</label>
        <?= \kartik\date\DatePicker::widget([
            'name' => 'start_date',
            'value' => $startDate,
            'options' => ['placeholder' => 'აირჩიე თარიღი ...'],
            'pluginOptions' => [
                'format' => 'yyyy-mm-dd',
                'todayHighlight' => true,
                'autoclose' => true

            ]
        ])?>
    </div>
</div>
<br>
<div class="row">
    <div class="col-md-4 col-md-offset-4">
        <label>ბოლო თარიღი</label>
        <?= \kartik\date\DatePicker::widget([
            'name' => 'end_date',
            'value' => $endDate,
            'options' => ['placeholder' => 'აირჩიე თარიღი ...'],
            'pluginOptions' => [
                'format' => 'yyyy-mm-dd',
                'todayHighlight' => true,
                'autoclose' => true
            ]
        ])?>
    </div>
</div>
<br>
<div class="row">
    <div class="col-md-4 col-md-offset-4">
        <label>კატეგორია</label>
        <?= \yii\helpers\Html::dropDownList('protocol_type',$selectedProtocol,$protocolTypes,[
            'class' => 'form-control'
        ]) ?>
    </div>
</div>
<br>
<br>
<div class="row">
    <div class="col-md-4 col-md-offset-4" style="text-align: center">
        <?= Html::submitButton('Filter  ', [
            'class' => 'btn btn-primary btn-block',
            'id' => 'excelButtonProtocolsDefault',
        ]) ?>
    </div>
</div>
<br>
<div class="row <?= (isset($protocols) && count($protocols) > 0) ? "" : " hide" ?>">
    <div class="col-md-4 col-md-offset-4" style="text-align: center">
        <?= Html::submitButton('Generate Excel  ', [
            'id' => 'excelButtonProtocols',
            'class' => 'btn btn-success btn-block',
        ]) ?>
    </div>
</div>
<br>

<?php ActiveForm::end(); ?>
<br><br><br>
<?php
if(isset($protocols)){
    echo "<h2>Out</h2>";
    echo $this->render('_form_table_protocols', [
        'protocols' => $protocols
    ]);
}
?>

