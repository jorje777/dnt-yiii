<?php
/**
 * Created by PhpStorm.
 * User: sai
 * Date: 8/21/17
 * Time: 12:06 PM
 * @author Saiat Kalbiev <kalbievich11@gmail.com>
 */

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $startTime  string */
/* @var $endTime string  */
/* @var $date string  */
/* @var $expenseData [] */
/* @var $drugs [] */
/* @var $drug_types [] */
/* @var $patients [] */
/* @var $selectedPatient string */
/* @var $selectedDrug string */

?>
<?php $form = ActiveForm::begin([
    'action' => '/dnt/filter/moh-time',
    'method' => "POST",
    'id' => 'mohTimeFilterForm'
]); ?>
<div class="row">
    <div class="col-md-4 col-md-offset-4">
        <label>დასაწყისი თარიღი</label>
        <?= \kartik\date\DatePicker::widget([
            'name' => 'date',
            'value' => $date,
            'options' => ['placeholder' => 'აირჩიე თარიღი ...'],
            'pluginOptions' => [
                'format' => 'yyyy-mm-dd',
                'todayHighlight' => true,
                'autoclose' => true
            ]
        ])?>
    </div>
</div>
<br>

<div class="row">
    <div class="col-md-2 col-md-offset-4">
        <?= \kartik\time\TimePicker::widget([
            'name' => 'startTime',
            'pluginOptions' => [
                'showSeconds' => false,
                'showMeridian' => false,
                'minuteStep' => 1,
                'defaultTime' => $startTime ? $startTime : false
            ]
        ])?>
    </div>
    <div class="col-md-2">
        <?= \kartik\time\TimePicker::widget([
            'name' => 'endTime',
            'pluginOptions' => [
                'showSeconds' => false,
                'showMeridian' => false,
                'minuteStep' => 1,
                'defaultTime' => $endTime ? $endTime : false
            ]
        ])?>
    </div>
</div>

<br>

<div class="row">
    <div class="col-md-4 col-md-offset-4">
        <?= \kartik\select2\Select2::widget([
            'data' => $patients,
            'name' => 'patient_id',
            'value' => $selectedPatient ? $selectedPatient : null ,
            'options' => [
                'placeholder' => 'აირჩიე პაციენტი ...',
            ],
            'pluginOptions' => [
                'allowClear' => true,
            ]
        ]) ?>
    </div>
</div>
<br>
<div class="row">
    <div class="col-md-4 col-md-offset-4">
        <label>Category</label>
        <?= \yii\helpers\Html::dropDownList('drug_type',$selectedDrug,$drug_types,[
            'id' => 'filterDrugType',
            'class' => 'form-control'
        ]) ?>
    </div>
</div>
<br>

<div class="row">
    <div class="col-md-4 col-md-offset-4" style="text-align: center">
        <?= Html::submitButton('Filter  ', [
            'class' => 'btn btn-primary btn-block',
            'id' => 'excelButtonMohDefaultTime'
        ]) ?>
    </div>
</div>
<br>
<div class="row <?= isset($expenseData) ? " " : " hide" ?>">
    <div class="col-md-4 col-md-offset-4" style="text-align: center">
        <?= Html::submitButton('Excel', [
            'class' => 'btn btn-success btn-block',
            'id' => 'excelButtonMohTime'
        ]) ?>
    </div>
</div>
<br>

<?php
if(isset($expenseData) && count($expenseData) > 0){
    echo $this->render('_form_table_moh_time_data', [
        'expenseData' => $expenseData,
        'drugs' => $drugs
    ]);
}
?>
<?php ActiveForm::end(); ?>
