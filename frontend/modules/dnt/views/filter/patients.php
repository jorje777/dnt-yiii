<?php
/**
 * Created by PhpStorm.
 * User: sai
 * Date: 8/14/17
 * Time: 5:52 PM
 * @author Saiat Kalbiev <kalbievich11@gmail.com>
 */


use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $patients [] */
/* @var $patientGroups [] */
/* @var $this \yii\web\View */
/* @var $out [] */
/* @var $startDate string */
/* @var $selectedDrugType string */
/* @var $endDate string */
/* @var $selectedPatient integer */
/* @var $selectedGroups [] */
/* @var $sumPrice integer */
/* @var $drug_types integer */
/* @var $bloodLabs [] */
/* @var $labs [] */


?>

<?php $form = ActiveForm::begin([
    'action' => '/dnt/filter/patients',
    'method' => "POST",
    'id' => "patientsFilterForm"
]); ?>

<div class="row">
    <div class="col-md-4 col-md-offset-4">
        <label>დასაწყისი თარიღი</label>
        <?= \kartik\date\DatePicker::widget([
            'name' => 'start_date',
            'value' => $startDate,
            'options' => ['placeholder' => 'აირჩიე თარიღი ...'],
            'pluginOptions' => [
                'format' => 'yyyy-mm-dd',
                'todayHighlight' => true,
                'autoclose' => true
            ]
        ])?>
    </div>
</div>
<br>
<div class="row">
    <div class="col-md-4 col-md-offset-4">
        <label>ბოლო თარიღი</label>
        <?= \kartik\date\DatePicker::widget([
            'name' => 'end_date',
            'value' => $endDate,
            'options' => ['placeholder' => 'აირჩიე თარიღი ...'],
            'pluginOptions' => [
                'format' => 'yyyy-mm-dd',
                'todayHighlight' => true,
                'autoclose' => true
            ]
        ])?>
    </div>
</div>
<br>
<div class="row">
    <div class="col-md-4 col-md-offset-4">
        <?= \kartik\select2\Select2::widget([
            'data' => $patients,
            'name' => 'patient',
            'value' => $selectedPatient ? $selectedPatient : null ,
            'options' => [
                'placeholder' => 'აირჩიე პაციენტი ...',
            ],
            'pluginOptions' => [
                'allowClear' => true,
            ]
        ]) ?>
    </div>
</div>
<br>
<div class="row">
    <div class="col-md-4 col-md-offset-4">
        <label>ჯგუფი</label>
        <?= \kartik\select2\Select2::widget([
            'data' => $patientGroups,
            'name' => 'patientGroups',
            'value' => $selectedGroups ? $selectedGroups : null,
            'options' => [
                'placeholder' => 'არჩიე ჯგიფი..',
            ],
            'pluginOptions' => [
                'allowClear' => true,
                'multiple' => true,

            ]
        ]) ?>
    </div>
</div>
<br>
    <div class="row">
        <div class="col-md-4 col-md-offset-4">
            <label>მარაგი</label>
            <?= \yii\helpers\Html::dropDownList('drug_type',$selectedDrugType,$drug_types,[
                'class' => 'form-control'
            ]) ?>
        </div>
    </div>
<br>
<div class="row">
    <div class="col-md-4 col-md-offset-4" style="text-align: center">
        <?= Html::submitButton('Filter  ', [
            'class' => 'btn btn-primary btn-block',
            'id' => 'excelButtonPatientsDefault'
        ]) ?>
    </div>
</div>
<br>
<div class="row <?= (isset($out) && count($out) > 0) ? "" : " hide" ?>">
    <div class="col-md-4 col-md-offset-4" style="text-align: center">
         <?= Html::submitButton('Generate Excel  ', [
             'id' => 'excelButtonPatients',
             'class' => 'btn btn-success btn-block',
         ]) ?>
    </div>
</div>
<?php ActiveForm::end(); ?>
<br><br><br>

<?php
if(isset($out) && count($out) > 0){
    echo $this->render('_form_table_patients', [
        'out' => $out,
        'sumPrice' => $sumPrice
    ]);
}
?>


<br>
<?php
if($labs && count($labs) > 0){
    echo "<h3>ლაბორატორიული კვლევა</h3>";
    echo $this->render('_form_table_patients_lab.php', [
        'labs' => $labs,
    ]);
}
?>
<br>
<?php
if(isset($bloodLabs) && count($bloodLabs) > 0){
    echo "<h3>სისძარღვოვანი მიდგომა</h3>";
    echo $this->render('_form_table_patients_blood_lab.php', [
        'bloodLabs' => $bloodLabs,
    ]);
}
?>
