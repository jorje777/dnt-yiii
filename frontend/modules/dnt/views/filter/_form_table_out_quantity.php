<?php
/**
 * Created by PhpStorm.
 * User: kalbievich
 * Date: 8/13/17
 * Time: 6:48 PM
 */

use frontend\modules\dnt\models\Stock;

/* @var $out [] */

$counter = 1;

?>

<table class="table table-bordered">
    <thead>
    <tr>
        <th>#</th>
        <th>პრეპარატი</th>
        <th>რაოდენობა</th>
        <th>ტიპი</th>
        <th>სეანსის თარიღი</th>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($out as $obj): ?>
        <tr>
            <td><?= $counter++ ?></td>
            <td><?= $obj->stock->drug->name?></td>
            <td><?= $obj->quantity ?></td>
            <td><?= $obj->stock->type ? Stock::getQuantityTypeConstants()[$obj->stock->type] : "" ?></td>
            <td><?= date('Y-m-d  g:i a',$obj->created_at) ?></td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>
