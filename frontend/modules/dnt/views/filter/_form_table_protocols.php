<?php
/**
 * Created by PhpStorm.
 * User: kalbievich
 * Date: 8/14/17
 * Time: 10:14 PM
 */

/* @var $protocols [] */

$counter = 1;
$sum = 0;
foreach ($protocols as $p){
    $sum += $p['quantity'];
}

?>

<table class="table table-bordered">
    <thead>
    <tr>
        <td></td>
        <td></td>
        <td></td>
        <th>ჯამი <?= $sum ?></th>
    </tr>
    <tr>
        <th>#</th>
        <th>სეანსის თარიღი</th>
        <th>გვარი, სახელი</th>
        <th>რაოდენობა</th>
    </tr>
    </thead>
    <tbody>
    <?php /** @var \frontend\modules\dnt\models\Expense $expense */
    foreach ($protocols as $protocol): ?>
        <tr>
            <td><?= $counter++ ?></td>
            <td><?= date('Y-m-d', $protocol['session_date']) ?></td>
            <td><?= $protocol['surname']. " " . $protocol['name']  ?></td>
            <td><?= $protocol['quantity'] ?></td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>
