<?php
/**
 * Created by PhpStorm.
 * User: kalbievich
 * Date: 8/13/17
 * Time: 6:48 PM
 */

use frontend\modules\dnt\models\Stock;

/* @var $out [] */

$counter = 0;

?>

<table class="table table-bordered">
    <thead>
    <tr>
        <th>#</th>
        <th>სეანსის თარიღი</th>
        <th>პრეპარატი</th>
        <th>რაოდენობა</th>
        <th>ტიპი</th>
        <th>ფასი</th>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($out as $obj): ?>
        <tr>
            <td><?= $counter++ ?></td>
            <td><?= date('Y-m-d  g:i a',$obj->session_date) ?></td>
            <td><?= $obj->stock->drug->name?></td>
            <td><?= $obj->quantity ?></td>
            <td><?= $obj->stock->type ? Stock::getQuantityTypeConstants()[$obj->stock->type] : ""  ?></td>
            <td><?= $obj->stock->price ?></td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>
