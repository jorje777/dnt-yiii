<?php
/**
 * Created by PhpStorm.
 * User: sai
 * Date: 8/16/17
 * Time: 4:08 PM
 * @author Saiat Kalbiev <kalbievich11@gmail.com>
 */

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $remainders [] */
/* @var $drugTypes [] */
/* @var $drugType integer */
/* @var $type string */
/* @var $message string */
/* @var $selectedDate string */

?>

<?php $form = ActiveForm::begin([
    'action' => '/dnt/filter/remainder',
    'method' => "POST",
    'id' => 'remainderFilterForm'
]); ?>

<div class="alert alert-<?= $type ? $type : " hide"?>" role="alert" id="generalAlertBox">
    <strong><?= $message ? $message : "" ?></strong>
</div>

<div class="row">
    <div class="col-md-3">
        <?= Html::dropDownList('drug_type',isset($drugType) ? $drugType : 0,$drugTypes,[
            'class' => 'form-control',
            'required' => true
        ]) ?>
    </div>
    <div class="col-md-3">
        <?= \kartik\date\DatePicker::widget([
            'name' => 'date',
            'value' => isset($selectedDate) ? $selectedDate : date('Y-m'),
            'options' => [
                'placeholder' => 'აირჩიე თარიღი ...',
                'required' => true

            ],
            'pluginOptions' => [
                'format' => 'yyyy-mm',
                'todayHighlight' => true,
                'minViewMode' => 1,
                'autoclose' => true
            ]
        ])?>
    </div>
    <div class="col-md-1" style="text-align: center">
        <?= Html::submitButton('ნახვა  ', [
            'class' => 'btn btn-primary',
            'id' => 'excelButtonRemainderDefault'
        ]) ?>
    </div>
    <div class="col-md-2 <?= (isset($remainders) && count($remainders) > 0) ? "" : " hide" ?>" style="text-align: center">
        <?= Html::submitButton('Generate Excel  ', [
            'id' => 'excelButtonRemainder',
            'class' => 'btn btn-success btn-block',
        ]) ?>
    </div>
</div>

<?php ActiveForm::end(); ?>
<br><br>
<div>
    <?php
    if(isset($remainders)){
        echo $this->render('_form_table_remainders', [
            'remainders' => $remainders
        ]);
    }
    ?>
</div>
