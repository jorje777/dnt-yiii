<?php
/**
 * Created by PhpStorm.
 * User: sai
 * Date: 8/21/17
 * Time: 1:34 PM
 * @author Saiat Kalbiev <kalbievich11@gmail.com>
 */

/* @var $expenseData [] */
/* @var $drugs [] */

?>


<table class="table table-bordered">
    <thead>
    <tr>
        <th style="text-align: center">
            <?= \yii\helpers\Html::checkbox(null,false,[
                'id' => 'mohAllCheckBox'
            ]) ?>
        </th>
        <th>პაციენტი</th>
        <?php foreach ($drugs as $drug) :?>
            <th>
                <?= $drug ?>
            </th>
        <?php endforeach; ?>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($expenseData as $patient): ?>
        <tr>
            <td style="text-align: center">
                <?= \yii\helpers\Html::checkbox('expense_ids[]',false,[
                        'value' => $patient['id'],
                        'class' => 'mohCheckboxes'
                ]) ?>
            </td>
            <td> <?= $patient['name'] ?></td>
            <?php
                foreach ($patient['drugs'] as $drugCount) {
                    echo "<td> $drugCount </td>";
                }
            ?>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>

