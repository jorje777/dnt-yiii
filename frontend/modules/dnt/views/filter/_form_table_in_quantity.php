<?php
/**
 * Created by PhpStorm.
 * User: kalbievich
 * Date: 8/13/17
 * Time: 6:48 PM
 */

use frontend\modules\dnt\models\Stock;

/* @var $in [] */

$counter = 1;

?>

<table class="table table-bordered">
    <thead>
        <tr>
            <th>#</th>
            <th>პრეპარატი</th>
            <th>რაოდენობა</th>
            <th>ტიპი</th>
            <th>შექმნის თარიღი</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($in as $obj): ?>
            <tr>
                <td><?= $counter++ ?></td>
                <td><?= $obj->drug->name ?></td>
                <td><?= $obj->initial_quantity ?></td>
                <td><?= $obj->type ? Stock::getQuantityTypeConstants()[$obj->type] : "" ?></td>
                <td><?= date('Y-m-d  g:i a',$obj->created_at) ?></td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>
