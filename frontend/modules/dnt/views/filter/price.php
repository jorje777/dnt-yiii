<?php
/**
 * Created by PhpStorm.
 * User: sai
 * Date: 8/14/17
 * Time: 4:33 PM
 * @author Saiat Kalbiev <kalbievich11@gmail.com>
 */

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $drugs [] */
/* @var $drug_types [] */
/* @var $this \yii\web\View */
/* @var $in [] */
/* @var $out [] */
/* @var $startDate integer */
/* @var $endDate integer */

?>

<?php $form = ActiveForm::begin([
    'action' => '/dnt/filter/price',
    'method' => "POST"
]); ?>

<div class="row">
    <div class="col-md-4 col-md-offset-4">
        <label>დასაწყისი თარიღი</label>
        <?= \kartik\date\DatePicker::widget([
            'name' => 'start_date',
            'value' => $startDate,
            'options' => ['placeholder' => 'აირჩიე თარიღი ...'],
            'pluginOptions' => [
                'format' => 'yyyy-mm-dd',
                'todayHighlight' => true,
                'autoclose' => true
            ]
        ])?>
    </div>
</div>
<br>
<div class="row">
    <div class="col-md-4 col-md-offset-4">
        <label>ბოლო თარიღი</label>
        <?= \kartik\date\DatePicker::widget([
            'name' => 'end_date',
            'value' => $endDate,
            'options' => ['placeholder' => 'აირჩიე თარიღი ...'],
            'pluginOptions' => [
                'format' => 'yyyy-mm-dd',
                'todayHighlight' => true,
                'autoclose' => true
            ]
        ])?>
    </div>
</div>
<br>
<div class="row">
    <div class="col-md-4 col-md-offset-4">
        <label>Category</label>
        <?= \yii\helpers\Html::dropDownList('drug_type',null,$drug_types,[
            'id' => 'filterDrugType',
            'class' => 'form-control'
        ]) ?>
    </div>
</div>
<br>
<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <?= \kartik\select2\Select2::widget([
            'data' => $drugs,
            'name' => 'drug[]',
            'options' => [
                'placeholder' => 'აირჩიე პრეპარატები...',
                'id' => "drug_id",
                'class' => 'random',
            ],
            'pluginOptions' => [
                'allowClear' => true,
                'multiple' => true,
            ]
        ]) ?>
    </div>
</div>
<br>
<div class="row">
    <div class="col-md-4 col-md-offset-4" style="text-align: center">
        <?= Html::submitButton('Filter  ', [
            'class' => 'btn btn-primary btn-block'
        ]) ?>
    </div>
</div>
<?php ActiveForm::end(); ?>
<br><br><br>

<?php
if(isset($in)){
    echo "<h1>In</h1>";
    echo $this->render('_form_table_in_price', [
        'in' => $in,
    ]);
}
?>
<?php
if(isset($out)){
    echo "<h2>Out</h2>";
    echo $this->render('_form_table_out_price', [
        'out' => $out
    ]);
}
?>
